import { StackNavigator } from "react-navigation";
import React from "react";
import { Image, TouchableOpacity } from "react-native";
import DashboardScreen from "../Views/CarListView/CarListView";
import LogoutScreen from "../Views/LogoutScreen/LogoutView";
import CarDetailsScreen from "../Views/CarListView/CarDetailsView";
import FilterScreen from "../Views/FilterScreen/FilterScreen";
import NotificationScreen from "../Views/NotificationView/Notification";
import UpdateProfileScreen from "../Views/UpdateProfile/UpdateProfile";
import { kThemeColor } from "./Styles";
import Login from "../Views/LoginScreen/LoginView";
import ForgotScreen from "../Views/LoginScreen/ForgotPassword";
import ResetPasswordScreen from "../Views/LoginScreen/ResetPassword";
//Screens
const LoginStack = StackNavigator(
  {
    LoginScreen: {
      screen: Login
    }
  },
  {
    navigationOptions: {
      header: null
    }
  }
);
const ForgotStack = StackNavigator(
  {
    ForgotScreen: {
      screen: ForgotScreen
    }
  },
  {
    navigationOptions: {
      header: null
    }
  }
);
const ResetPasswordStack = StackNavigator(
  {
    ResetPasswordScreen: {
      screen: ResetPasswordScreen
    }
  },
  {
    navigationOptions: {
      header: null
    }
  }
);
const DashboardStack = StackNavigator(
  {
    DashboardScreen: {
      screen: DashboardScreen
    }
  },
  {
    navigationOptions: {
      header: null
    }
  }
);
//Car details view
const CarDetailsStack = StackNavigator(
  {
    CarDetailsScreen: {
      screen: CarDetailsScreen
    }
  },
  {
    navigationOptions: {
      header: null
    }
  }
);

const FilterScreenStack = StackNavigator(
  {
    FilterScreen: {
      screen: FilterScreen
    }
  },
  {
    navigationOptions: {
      header: null
    }
  }
);
const NotificationScreenStack = StackNavigator(
  {
    NotificationScreen: {
      screen: NotificationScreen
    }
  },
  {
    navigationOptions: {
      header: null
    }
  }
);
const UpdateProfileScreenStack = StackNavigator(
  {
    UpdateProfileScreen: {
      screen: UpdateProfileScreen
    }
  },
  {
    navigationOptions: {
      header: null
    }
  }
);
const LogoutScreenStack = StackNavigator(
  {
    LogoutScreen: {
      screen: LogoutScreen
    }
  },
  {
    navigationOptions: {
      header: null
    }
  }
);
//export default (RootForgotStack);
export default (Root = StackNavigator(
  {
    Login: {
      path: "/login",
      screen: LoginStack,
      navigationOptions: ({ navigation }) => ({ header: null })
    },
    Dashboard: {
      path: "/dashboard",
      screen: DashboardStack,
      navigationOptions: ({ navigation }) => ({ header: null })
      // navigationOptions: ({ navigation }) => {
      //   return {
      //     title: "Dashboard",
      //     headerRight: (
      //       <TouchableOpacity
      //         onPress={() => navigation.navigate("FilterScreen")}
      //       >
      //         <Image
      //           source={require("../assets/Images/filter.png")}
      //           style={{
      //             width: 20,
      //             height: 20,
      //             margin: 12
      //           }}
      //         />
      //       </TouchableOpacity>
      //     ),
      //     headerLeft: (
      //       <TouchableOpacity
      //         onPress={() => navigation.navigate("Notification")}
      //       >
      //         <Image
      //           source={require("../assets/Images/notification.png")}
      //           style={{
      //             width: 20,
      //             height: 20,
      //             margin: 12
      //           }}
      //         />
      //       </TouchableOpacity>
      //     ),
      //     headerStyle: {
      //       backgroundColor: kThemeColor
      //     },
      //     headerTintColor: "#fff",
      //     headerTitleStyle: {
      //       fontFamily: "Muli-SemiBold",
      //       fontWeight: "100"
      //     }
      //   };
      // }
    },
    ForgotEMail: {
      path: "/forgotEmail",
      screen: ForgotStack,
      navigationOptions: {
        title: "Forgot Password",
        headerStyle: {
          backgroundColor: kThemeColor
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "Muli-SemiBold",
          fontWeight: "100"
        }
      }
    },
    ResetPassword: {
      path: "/resetPassword",
      screen: ResetPasswordStack,
      navigationOptions: ({ navigation }) => ({ header: null })
    },
    CarDetails: {
      path: "/carDetails",
      screen: CarDetailsStack,
      navigationOptions: ({ navigation }) => ({ header: null })
    },

    FilterScreen: {
      path: "/filterScreen",
      screen: FilterScreenStack,
      navigationOptions: {
        title: "Filter",
        headerStyle: {
          backgroundColor: kThemeColor
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "Muli-SemiBold",
          fontWeight: "100"
        }
      }
    },
    Notification: {
      path: "/notificationScreen",
      screen: NotificationScreenStack,
      navigationOptions: {
        title: "Notification",
        headerStyle: {
          backgroundColor: kThemeColor
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "Muli-SemiBold",
          fontWeight: "100"
        }
      }
    },
    UpdateProfile: {
      path: "/updateProfile",
      screen: UpdateProfileScreenStack,
      navigationOptions: {
        headerTitle: "Update Profile",
        headerStyle: {
          backgroundColor: kThemeColor
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "Muli-SemiBold",
          fontWeight: "100"
        }
      }
    },
    Logout: {
      path: "/Logout",
      screen: LogoutScreenStack,
      navigationOptions: {
        header: null
      }
    }
  },
  {
    initialRouteName: "Dashboard"
    // contentOptions: {
    //   inactiveTintColor: "#002963",
    //   activeTintColor: "#002963"
    // }
    //contentComponent: props => <Text>Hello</Text>
  }
));
