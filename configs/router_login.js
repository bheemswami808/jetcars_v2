import { createStackNavigator } from "react-navigation";
import React from "react";
import { Image, TouchableOpacity } from "react-native";
import Login from "../Views/LoginScreen/LoginView";
import ForgotScreen from "../Views/LoginScreen/ForgotPassword";
import DashboardScreen from "../Views/CarListView/CarListView";
import ResetPasswordScreen from "../Views/LoginScreen/ResetPassword";
import CarDetailsScreen from "../Views/CarListView/CarDetailsView";
import FilterScreen from "../Views/FilterScreen/FilterScreen";
import UpdateProfileScreen from "../Views/UpdateProfile/UpdateProfile";
import NotificationScreen from "../Views/NotificationView/Notification";

import { kThemeColor, styles } from "./Styles";
import Logout from "../Views/LogoutScreen/LogoutView";
//Screens
const LogoutStack = createStackNavigator(
  {
    LogoutScreen: {
      screen: Logout
    }
  },
  {
    navigationOptions: {
      header: null
    }
  }
);

const LoginStack = createStackNavigator(
  {
    LoginScreen: {
      screen: Login
    }
  },
  {
    navigationOptions: {
      header: null
    }
  }
);
const ForgotStack = createStackNavigator(
  {
    ForgotScreen: {
      screen: ForgotScreen
    }
  },
  {
    navigationOptions: {
      header: null
    }
  }
);
const DashboardStack = createStackNavigator(
  {
    DashboardScreen: {
      screen: DashboardScreen
    }
  },
  {
    navigationOptions: {
      header: null
    }
  }
);
//Car details view
const CarDetailsStack = createStackNavigator(
  {
    CarDetailsScreen: {
      screen: CarDetailsScreen
    }
  },
  {
    navigationOptions: {
      header: null
    }
  }
);

// reset password stack
const ResetPasswordStack = createStackNavigator(
  {
    ResetPasswordScreen: {
      screen: ResetPasswordScreen
    }
  },
  {
    navigationOptions: {
      header: null
    }
  }
);
const FilterScreenStack = createStackNavigator(
  {
    FilterScreen: {
      screen: FilterScreen
    }
  },
  {
    navigationOptions: {
      header: null
    }
  }
);

const UpdateProfileScreenStack = createStackNavigator(
  {
    UpdateProfileScreen: {
      screen: UpdateProfileScreen
    }
  },
  {
    navigationOptions: {
      header: null
    }
  }
);
const NotificationScreenStack = createStackNavigator(
  {
    NotificationScreen: {
      screen: NotificationScreen
    }
  },
  {
    navigationOptions: {
      header: null
    }
  }
);

//export default (RootForgotStack);
export default (Root = createStackNavigator(
  {
    Logout: {
      path: "/logout",
      screen: LogoutStack,
      navigationOptions: ({ navigation }) => ({ header: null })
    },
    Login: {
      path: "/login",
      screen: LoginStack,
      navigationOptions: ({ navigation }) => ({ header: null })
    },
    Dashboard: {
      path: "/dashboard",
      screen: DashboardStack,
      navigationOptions: ({ navigation }) => ({ header: null })
    },

    ResetPassword: {
      path: "/resetPassword",
      screen: ResetPasswordStack,
      navigationOptions: ({ navigation }) => ({ header: null })
    },
    ForgotEMail: {
      path: "/forgotEmail",
      screen: ForgotStack,
      navigationOptions: {
        title: "Forgot Password",
        headerStyle: {
          backgroundColor: kThemeColor
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "Muli-SemiBold",
          fontWeight: "100"
        }
      }
    },
    CarDetails: {
      path: "/carDetails",
      screen: CarDetailsStack,
      navigationOptions: ({ navigation }) => ({ header: null })
    },
    Notification: {
      path: "/notificationScreen",
      screen: NotificationScreenStack,
      navigationOptions: {
        title: "Notification",
        headerStyle: {
          backgroundColor: kThemeColor
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "Muli-SemiBold",
          fontWeight: "100"
        }
      }
    },

    FilterScreen: {
      path: "/filterScreen",
      screen: FilterScreenStack,
      navigationOptions: {
        title: "Filter",
        headerStyle: {
          backgroundColor: kThemeColor
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "Muli-SemiBold",
          fontWeight: "100"
        }
      }
    },
    UpdateProfile: {
      path: "/updateProfile",
      screen: UpdateProfileScreenStack,
      navigationOptions: {
        headerTitle: "Update Profile",
        headerStyle: {
          backgroundColor: kThemeColor
        },
        headerTintColor: "#fff",
        headerTitleStyle: {
          fontFamily: "Muli-SemiBold",
          fontWeight: "100"
        }
      }
    }
  },

  {
    initialRouteName: "Login"
    // contentOptions: {
    //   inactiveTintColor: "#002963",
    //   activeTintColor: "#002963"
    // }
    //contentComponent: props => <Text>Hello</Text>
  }
));
