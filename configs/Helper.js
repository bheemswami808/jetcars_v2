const BASE_PATH = "http://103.207.170.205/jetcars/public/api/";
const dd = data => {
  console.log(data);
};
function getImgSrcSingle(imgArr) {
  if (imgArr === undefined || imgArr.length === 0) {
    return "../assets/images/splash_img.png";
  }
  return imgArr[0];
}
function getImgSrcAll(imgArr) {
  if (imgArr === undefined || imgArr.length === 0) {
    return "../assets/images/splash_img.png";
  }
  return imgArr;
}
function getYearRange(frm = new Date().getFullYear(), to = 1980) {
  var years = [];
  for (var i = frm; i >= to; i--) {
    years.push({ id: i, name: i });
  }
  return years;
}
function objLength(obj) {
  if (obj != undefined) return Object.keys(obj).length;

  return 0;
}
function commentPaginator(items, page, per_page) {
  let totalItems = items.length;
  let remItem = per_page % 5;
  if (remItem > 0) {
  }
  return items.slice(page, per_page).reverse();
}
export default {
  BASE_PATH,
  dd,
  getImgSrcSingle,
  getImgSrcAll,
  getYearRange,
  objLength,
  commentPaginator
};
