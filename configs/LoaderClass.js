import React, { Component } from "react";
import { View, ActivityIndicator } from "react-native";

class LoaderClass extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.loader = this.loader.bind(this);
  }
  componentDidMount() {
    this.props.onRef(this);
  }
  loader(status) {
    this.setState({
      loader: status
    });
  }
  render = () => {
    return this.state.loader ? (
      <View
        style={{
          //flex: 1,
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: "transparent",
          //height: 60,
          opacity: 1,
          position: "absolute",
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          margin: "auto",
          zIndex: 999
        }}
      >
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    ) : null;
  };
}

export default LoaderClass;
