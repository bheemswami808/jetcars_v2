import React, { Component } from "react";
import axios from "axios";
import querystring from "querystring";
import Helper from "./Helper";
const BASE_ROOT = Helper.BASE_PATH;

// axios.interceptors.request.use(
//   function(config) {
//     return config;
//   },
//   function(error) {
//     // Do something with request error
//     return Promise.reject(error);
//   }
// );

//Response Interceptor
axios.interceptors.response.use(
  function(response) {
    return response;
  },
  function(error) {
    alert(error);
    //return Promise.reject(error.response);
  }
);

const requests = {
  get: url => axios.get(`${BASE_ROOT}${url}`),
  put: (url, body) => axios.post(`${BASE_ROOT}${url}`, body),
  post: (url, body) =>
    axios.post(`${BASE_ROOT}${url}`, querystring.stringify(body)),
  postWithFile: (url, body) => axios.post(`${BASE_ROOT}${url}`, body)
};
const UserApi = {
  loginUser: data => requests.post("login", data),
  logoutUser: userId => requests.get("logout/" + userId),
  forgotPassword: data => requests.post("forgot-password", data),
  updatePassword: data => requests.post("update-password", data),
  updateProfile: data => requests.post("update-profile", data)
};
const CarAPIs = {
  getCarList: data => requests.post("search-cars", data),
  getCarModals: data => requests.post("get-model", data),
  getDropdownData: () => requests.get("get-make"),
  getCarDetails: data => requests.post("car-details", data),
  sendUserCarComments: data => requests.post("save-user-comment", data),
  notifyMeApi: data => requests.post("notify-me", data),
  changeVehicleStatus: data => requests.post("change-vehicle-status", data)
};

const NotificationApi = {
  notificationList: data => requests.post("notification-list", data),
  VehicleDetail: data => requests.post("vehicle-detail", data)
};
export default {
  UserApi,
  CarAPIs,
  NotificationApi
};
