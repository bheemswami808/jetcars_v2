import { StyleSheet, Dimensions } from "react-native";
var grey = "#E0E0E0";
var darkGrey = "#586261";
var kThemeColor = "#003a5f";
var kThemeColorLight = "#3b98d3";
var white = "#FFF";
var black = "#000";
var transparent = "transparent";
const { width, height } = Dimensions.get("window");
//Styles starts from here
const styles = StyleSheet.create({
  wrapper: {
    backgroundColor: "#fff",
    fontFamily: "Muli-Regular"
  },
  headings: {
    fontWeight: "900",
    fontFamily: "Muli-SemiBold"
  },
  headerText: {
    fontWeight: "400",
    fontFamily: "Muli-Regular",
    fontSize: 19,
    marginRight: 20,
    color: white
  },
  blackText: {
    color: black,
    fontFamily: "Muli-Regular"
  },
  marginTop10: {
    marginTop: 10
  },
  marginTop20: {
    marginTop: 20
  },
  paddingBtm20: {
    paddingBottom: 20
  },
  paddingTop20: {
    paddingTop: 20
  },
  paddingBtm10: {
    paddingBottom: 10
  },
  paddingTop10: {
    paddingTop: 10
  },
  paddingBtm5: {
    paddingBottom: 5
  },
  paddingTop5: {
    paddingTop: 5
  },
  whiteText: {
    color: white,
    fontFamily: "Muli-Regular"
  },
  alignLeft: {
    alignSelf: "flex-start",
    flex: 1,
    paddingLeft: 10,
    paddingTop: 10
  },
  alignRight: {
    alignSelf: "flex-end",
    flex: 1,
    paddingRight: 10,
    alignItems: "flex-end",
    paddingTop: 10
  },
  textinput: {
    marginHorizontal: 20,
    backgroundColor: "white",
    height: 50,
    paddingLeft: 20,
    fontFamily: "Muli-Regular"
  },
  directionRow: {
    flexDirection: "row"
  },
  directionColumn: {
    flexDirection: "column"
  },
  topRadius: {
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10
  },
  bottomRadius: {
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10
  },
  topBorder: {
    borderTopWidth: 1,
    borderTopColor: grey
  },
  buttonStyle: {
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 20,
    width: width - 40,
    height: 45,
    borderRadius: 10,
    backgroundColor: white,
    borderBottomWidth: 3,
    borderBottomColor: kThemeColorLight
  },
  footerButtonTextStyle: {
    fontSize: 16,
    color: kThemeColor,
    fontFamily: "Muli-Regular"
  },
  headerImage: {
    resizeMode: "contain",
    width: width * 0.25,
    height: width * 0.25
  },
  backgroundImageView: {
    position: "absolute",
    left: 0,
    top: 0,
    width: "100%",
    height: "100%",
    resizeMode: "cover"
  },
  anchorTextStyle: {
    color: white,
    fontSize: 15,
    alignSelf: "center",
    marginTop: 10,
    fontFamily: "Muli-Regular"
  },
  blueBackground: {
    backgroundColor: "#003a5f09"
  }
});
const LoginViewStyle = StyleSheet.create({
  mainView: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: kThemeColor
  },
  textInputsView: {
    justifyContent: "flex-start",
    flexDirection: "column",
    height: 150,
    width: width
  },
  topLogo: {
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    width: width,
    height: height * 0.33
  },
  mainPageView: {
    justifyContent: "center",
    alignItems: "center",
    borderRadius: (width * 0.3) / 2,
    borderWidth: 1,
    borderColor: white,
    width: width * 0.3,
    height: width * 0.3,
    backgroundColor: white,
    overflow: "hidden"
  },
  footerButtonView: {
    justifyContent: "flex-start",
    flexDirection: "column",
    height: height * 0.3,
    width: width,
    marginTop: -20
  },
  footerImage: {
    width: width,
    height: width * 0.37,
    resizeMode: "center",
    position: "absolute",
    bottom: 0,
    alignSelf: "flex-end"
  }
});
const ForgotViewStyle = StyleSheet.create({
  mainView: {
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: kThemeColor
  },
  mainPageView: {
    justifyContent: "flex-start",
    flexDirection: "column",
    backgroundColor: transparent,
    height: height * 0.33
  },
  textInputsView: {
    justifyContent: "flex-start",
    flexDirection: "column",
    height: 80,
    width: width
  },
  loginButtonStyle: {
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 20,
    width: width - 40,
    height: 45,
    borderRadius: 22.5,
    backgroundColor: "grey"
  },
  footerButtonView: {
    justifyContent: "flex-start",
    flexDirection: "column",
    height: height * 0.33,
    width: width
  }
});
const SplashScreenStyle = StyleSheet.create({
  mainView: {
    flex: 11,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
  },
  image: {
    width: "40%",
    alignSelf: "center",
    resizeMode: "contain"
  }
});
const CarListStyle = StyleSheet.create({
  card: {
    flex: 1,
    borderColor: kThemeColor,
    borderWidth: 1,
    borderRadius: 5,
    margin: 10,
    //alignSelf: "center",
    // height: 400,
    fontFamily: "Muli",
    flexDirection: "column",
    paddingBottom: 20
  },
  titleBarParent: {
    backgroundColor: kThemeColor,
    flexDirection: "row"
  },
  title: {
    alignSelf: "flex-start",
    flex: 1
  },
  titleText: {
    fontSize: 16,
    padding: 10
  },
  comment: {
    alignSelf: "flex-end",
    flexDirection: "row",
    padding: 10
  },
  chatIcon: {
    height: 20,
    width: 20,
    marginRight: 10
  },
  sliderParent: {
    height: width * 0.5,
    backgroundColor: white,
    overflow: "hidden"
  },
  imageSlide: {
    width: "100%",
    height: "100%",
    resizeMode: "contain"
  },
  detailParent: {
    //backgroundColor: "red",
    // height: width * 0.5,
    borderTopColor: darkGrey,
    borderTopWidth: 1,
    flexDirection: "column"
  },
  grayTimeText: {
    marginTop: 8,
    color: darkGrey,
    fontSize: 14
  },
  calenderIcon: {
    height: 14,
    width: 14,
    marginTop: 10,
    marginRight: 10
  }
});
///Cars details style
const CarDetailsStyle = StyleSheet.create({
  mainView: {
    flex: 1,
    backgroundColor: "white"
  },
  mainPageView: {
    justifyContent: "flex-start",
    flexDirection: "column",
    backgroundColor: "white",
    height: height * 0.3
  },
  headTitleTexts: {
    flex: 1,
    flexDirection: "row"
  },
  lineView: {
    backgroundColor: darkGrey,
    height: 1,
    marginHorizontal: 20,
    marginTop: 5
  },
  carDetailsView: {
    justifyContent: "flex-start",
    flexDirection: "column"
  },
  DetailsTextStyle: {
    marginHorizontal: 20,
    marginTop: 20,
    flexDirection: "row",
    flex: 1,
    backgroundColor: "white",
    color: darkGrey
  },
  mutliItmesView: {
    flex: 1,
    marginTop: 20,
    marginHorizontal: 20,
    justifyContent: "center",
    flexDirection: "row"
  },
  multiTextview: {
    flex: 1,

    backgroundColor: "white",
    color: darkGrey
  },
  HeaderTextStyle: {
    marginHorizontal: 10,
    marginTop: 20,
    marginBottom: 10,
    backgroundColor: "white",
    color: kThemeColor,
    fontFamily: "Muli-Bold"
  },
  VehicalDetailsStyle: {
    marginTop: -1,
    marginHorizontal: 20,
    backgroundColor: "black",
    flexDirection: "row",
    overflow: "hidden",
    borderColor: "black",
    borderWidth: 1
  },
  VehicalDetailsInViewTextStyle: {
    backgroundColor: "white",
    flex: 1,
    padding: 5
  },
  messageViewStyle: {
    position: "absolute",
    width: "100%",
    height: 60,
    backgroundColor: grey,
    bottom: 0,
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  MessageTextStyle: {
    height: 40,
    flex: 2,
    padding: 10,
    marginHorizontal: 20,
    backgroundColor: "white",
    borderRadius: 20
  },
  sendButtonStyle: {
    height: 40,
    width: 40,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "white",
    marginRight: 20,
    borderRadius: 20,
    overflow: "hidden"
  },
  sendButtonImageStyle: {
    height: "100%",
    width: "100%",
    //backgroundColor: "red",
    resizeMode: "cover"
  },
  footerButtonView: {
    justifyContent: "flex-start",
    flexDirection: "column",
    height: height * 0.33,
    width: width
  },
  modalStyle: {
    flex: 1,
    margin: 40,
    backgroundColor: kThemeColor,
    flexDirection: "column",
    marginTop: 65
  },
  modalButton: {
    width: "80%",
    alignSelf: "center",
    margin: 20
    //position: "absolute",
    //bottom: 10
  }
});

//comments
const UserCommentsStyle = StyleSheet.create({
  card: {
    marginHorizontal: 15,
    marginTop: 10,
    flexDirection: "column",
    flex: 1,
    overflow: "hidden"
  },
  Commentscard: {
    marginLeft: 5,
    marginRight: 40,
    flex: 1,
    backgroundColor: grey,
    borderRadius: 30,
    overflow: "hidden",
    justifyContent: "center",
    alignSelf: "flex-start"
  },
  userNameText: {
    marginRight: 40,
    flex: 1,
    marginVertical: 5,
    fontSize: 13,
    fontFamily: "Muli-Regular"
  },
  userCommentText: {
    marginHorizontal: 20,
    marginVertical: 10,
    fontSize: 15,
    fontFamily: "Muli-Regular"
  },
  userCommentDateText: {
    marginHorizontal: 5,
    marginVertical: 2,
    fontSize: 12,
    fontFamily: "Muli-Light",
    color: "grey"
  }
});
const Service = StyleSheet.create({
  TextPopup: {
    fontWeight: "bold",
    fontFamily: "Muli-Bold"
  },
  options: {
    padding: 15,
    fontSize: 16,
    fontFamily: "Muli-Regular"
  }
});
//filter screen
const CarFilterStyle = StyleSheet.create({
  mainView: {
    backgroundColor: white,
    paddingVertical: 20
  },
  selectToggle: {
    borderWidth: 1.5,
    borderColor: darkGrey,
    backgroundColor: "white",
    height: 40,
    paddingHorizontal: 10,
    fontFamily: "Muli-Regular"
  },
  selectView: {
    justifyContent: "flex-start",
    flexDirection: "row",
    flex: 1,
    margin: 10
  },
  selectSubView: {
    justifyContent: "flex-start",
    flex: 1
  },
  buttonView: {},
  buttonStyle: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
    width: width - 50,
    height: 45,
    backgroundColor: white,
    borderWidth: 2,
    borderColor: darkGrey
  },

  serachButtonTextStyle: {
    fontSize: 18,
    fontWeight: "100",
    color: darkGrey,
    fontFamily: "Muli-Regular",
    padding: 5
  }
});

const NotificationStyle = StyleSheet.create({
  mainView: {
    flex: 1,
    justifyContent: "flex-start",
    backgroundColor: "white"
  },
  flatList: {
    // padding: 20
  }
});
const FormStyle = StyleSheet.create({
  textInputsView: {
    justifyContent: "flex-start",
    flexDirection: "column"
  },
  label: {
    paddingLeft: 20,
    fontFamily: "Muli-Semibold",
    marginTop: 9,
    fontSize: 15
  },
  textinput: {
    marginHorizontal: 20,
    backgroundColor: "white",
    height: 48,
    paddingLeft: 20,
    fontFamily: "Muli-Regular",
    borderWidth: 1,
    borderColor: grey,
    borderRadius: 5,
    marginTop: 5
  },
  btnView: {
    justifyContent: "flex-start",
    flexDirection: "column",
    marginBottom: 20
  },
  btn: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
    height: 45,
    backgroundColor: kThemeColor,
    borderRadius: 5
  },
  btnText: {
    fontFamily: "Muli-Regular",
    color: "#fff",
    fontWeight: "600",
    fontSize: 16
  }
});
//Styles end here
//Export styles
export {
  styles,
  LoginViewStyle,
  SplashScreenStyle,
  CarListStyle,
  ForgotViewStyle,
  CarDetailsStyle,
  width,
  height,
  kThemeColor,
  UserCommentsStyle,
  CarFilterStyle,
  Service,
  NotificationStyle,
  FormStyle
};
