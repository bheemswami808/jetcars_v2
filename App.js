import React, { Component } from "react";
import { AppRegistry } from "react-native";
import firebase from "react-native-firebase";
import type { Notification, NotificationOpen } from "react-native-firebase";
import SplashScreen from "./Views/SplashScreen/SplashScreen";
import CarListView from "./Views/CarListView/CarListView";
import Login from "./Views/LoginScreen/LoginView";
import Root from "./configs/router";
import RootLogin from "./configs/router_login";
import Store from "react-native-simple-store";
import FilterScreen from "./Views/FilterScreen/FilterScreen";

export default class jetcars extends Component {
  constructor(props) {
    super(props);
    this.state = {
      car_details: false,
      splashEnd: false,
      token: "",
      isLoggedIn: false
    };
    console.disableYellowBox = true;
  }

  async componentDidMount() {
    Store.get("auth").then(res => {
      global.userDetails = res;
      if (global.userDetails) {
        this.setState({ isLoggedIn: true });
      }
    });

    let flag = false;
    global.detailsVehicleId = 0;
    setTimeout(() => {
      this.setState({
        splashEnd: true
      });
    }, 2000);

    const notificationOpen: NotificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    if (notificationOpen) {
      const action = notificationOpen.action;
      const notification: Notification = notificationOpen.notification;
      flag = true;
      global.detailsVehicleId = notification.data.id;
    }

    this.notificationListener = firebase
      .notifications()
      .onNotification((notification: Notification) => {
        notification.android
          .setChannelId("jetCar-channel")
          .setSound("default")
          .android.setColor("#ffffff")
          .android.setSmallIcon("ic_launcher");
        firebase.notifications().displayNotification(notification);
      });

    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened((notificationOpen: NotificationOpen) => {
        const notification: Notification = notificationOpen.notification; // Get information about the notification that was opened
        flag = true;
        global.detailsVehicleId = notification.data.id;
        firebase
          .notifications()
          .removeDeliveredNotification(notification.notificationId);
      });
    this.setState({ car_details: flag });
    firebase
      .messaging()
      .getToken()
      .then(token => {
        if (token) {
          // console.log(token);
          global.deviceToken = token;
          this.setState({ token });
        }
      });
    firebase.messaging().onTokenRefresh(token => {
      if (token) {
        global.deviceToken = token;
      }
    });
  }
  componentWillUnmount() {
    this.notificationDisplayedListener();
    this.notificationListener();
    this.notificationOpenedListener();
  }
  render() {
    //if (this.state.isLoggedIn && this.state.car_details) {
    // return <CarListView />;
    //}
    return this.state.splashEnd ? (
      this.state.isLoggedIn ? (
        <Root />
      ) : (
        <RootLogin />
      )
    ) : (
      <SplashScreen state={this.state} />
    );
  }
}
AppRegistry.registerComponent("jetcars", () => jetcars);
