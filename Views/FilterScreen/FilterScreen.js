import React, { Component } from "react";
import {
  View,
  Text,
  TouchableHighlight,
  ScrollView,
  TextInput
} from "react-native";
import { kThemeColor, CarFilterStyle } from "../../configs/Styles";
import SectionedMultiSelect from "react-native-sectioned-multi-select";
import DropdownAlert from "react-native-dropdownalert";
import Agent from "../../configs/Agent";
import Helper from "../../configs/Helper";
import MultiSlider from "@ptomasroos/react-native-multi-slider";
import LoaderClass from "../../configs/LoaderClass";
import { Dimensions } from "react-native";
const { width, height } = Dimensions.get("window");
export default class FilterScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      is_advance_search: false,
      selectedMake: [],
      selectedModal: [],
      selectedType: [],
      selectedFuelVarient: [],
      selectedTransmissionType: [],
      selectedYears: [],
      selectedPower: [],
      selectedDoors: [],
      selectedColors: [],
      selectedOwners: [],
      selectedParking: [],

      listMake: [],
      listModal: [],
      listType: [],
      listFuelVarient: [],
      listTransmissionType: [],
      listYears: [],
      listPower: [],
      listDoors: [],
      listColors: [],
      listOwners: [],
      listParking: [],

      minPrice: "",
      maxPrice: "",
      sliderInitialVals: [0, 1000]
    };
    this.handleSearch = this.handleSearch.bind(this);
    this.advanceSearch = this.advanceSearch.bind(this);
    this.getModalDropdownList = this.getModalDropdownList.bind(this);
    this.multiSliderValuesChange = this.multiSliderValuesChange.bind(this);
  }

  componentDidMount() {
    Agent.CarAPIs.getDropdownData().then(response => {
      if (response.data.status) {
        let data = response.data.data;

        this.setState({
          sliderInitialVals: [
            data.price_rang[0].name.split("-")[0],
            data.price_rang[0].name.split("-")[1]
          ],
          listMake: [{ name: "Select Make", id: "id1", children: data.make }],
          listType: [
            { name: "Select Type", id: "id2", children: data.category }
          ],
          listFuelVarient: [
            { name: "Select Fuel Varient", id: "id3", children: data.fuel_type }
          ],
          listTransmissionType: [
            {
              name: "Select Transmission Type",
              id: "id4",
              children: data.transmission
            }
          ],
          listYears: [
            {
              name: "Select Year",
              id: "id5",
              children: Helper.getYearRange()
            }
          ],
          listPower: [
            { name: "Select Power", id: "id6", children: data.power }
          ],
          listDoors: [
            { name: "Select Door", id: "id7", children: data.door_count }
          ],
          listColors: [
            { name: "Select Color", id: "id8", children: data.body_color }
          ],
          listOwners: [
            { name: "Select Owner", id: "id9", children: data.owner_type }
          ],
          listParking: [
            { name: "Select Parking", id: "id10", children: data.parking }
          ]
        });
      } else {
        this.dropdown.alertWithType("error", "Error", response.data.msg);
      }
    });
  }
  advanceSearch() {
    this.setState({ is_advance_search: !this.state.is_advance_search });
  }
  handleSearch() {
    if (
      this.state.is_advance_search &&
      Number(this.state.minPrice) >= Number(this.state.maxPrice)
    ) {
      this.dropdown.alertWithType(
        "warn",
        "Warning",
        "Enter Maximun value more than the minimum value"
      );
    } else {
      let postData = {};
      let postPushData = [];

      if (this.state.selectedMake.length > 0) {
        postData.make_id = JSON.stringify(this.state.selectedMake);
        postPushData.push(this.state.filterSelectedDataMake);
      }
      if (this.state.selectedModal.length > 0) {
        postData.model_id = JSON.stringify(this.state.selectedModal);
        postPushData.push(this.state.filterSelectedDataModel);
      }
      if (this.state.selectedType.length > 0) {
        postData.category = JSON.stringify(this.state.selectedType);
        postPushData.push(this.state.filterSelectedDataCategory);
      }
      if (this.state.selectedFuelVarient.length > 0) {
        postData.fuel_type = JSON.stringify(this.state.selectedFuelVarient);
        postPushData.push(this.state.filterSelectedDataFuelType);
      }
      if (this.state.minPrice > 0) {
        postData.min_price = this.state.minPrice;
        postPushData.push({ keyName: "Min Price", value: this.state.minPrice });
      }
      if (this.state.maxPrice > 0) {
        postData.max_price = this.state.maxPrice;
        postPushData.push({ keyName: "Max Price", value: this.state.maxPrice });
      }
      if (this.state.selectedYears.length > 0) {
        postData.year = JSON.stringify(this.state.selectedYears);
        postPushData.push(this.state.filterSelectedDataYear);
      }
      if (this.state.selectedPower.length > 0) {
        postData.power = JSON.stringify(this.state.selectedPower);
        postPushData.push(this.state.filterSelectedDataPower);
      }
      if (this.state.selectedDoors.length > 0) {
        postData.door_count = JSON.stringify(this.state.selectedDoors);
        postPushData.push(this.state.filterSelectedDataDoor);
      }
      if (this.state.selectedColors.length > 0) {
        postData.color = JSON.stringify(this.state.selectedColors);
        postPushData.push(this.state.filterSelectedDataColor);
      }
      if (this.state.selectedOwners.length > 0) {
        postData.previous_owners = JSON.stringify(this.state.selectedOwners);
        postPushData.push(this.state.filterSelectedDataOwners);
      }
      if (this.state.selectedParking.length > 0) {
        postData.parking = JSON.stringify(this.state.selectedParking);
        postPushData.push(this.state.filterSelectedDataParking);
      }
      if (this.state.selectedTransmissionType.length > 0) {
        postData.parking = JSON.stringify(this.state.selectedTransmissionType);
        postPushData.push(this.state.filterSelectedDataTransmissionType);
      }
      this.loaderclass.loader(true);
      Agent.CarAPIs.getCarList(postData).then(response => {
        this.loaderclass.loader(false);
        if (response.data.status) {
          this.props.navigation.navigate("DashboardScreen", {
            isViewOpen: "list",
            carListData: response.data.data
          });
        } else {
          this.props.navigation.navigate("DashboardScreen", {
            filterPushData: postPushData,
            postData: postData,
            isViewOpen: "notify"
          });
        }
      });
    }
  }

  /* -----  Start Functions :: set filterd values by id ----- */
  multiSliderValuesChange = values => {
    this.setState({
      sliderInitialVals: values,
      minPrice: Number(values[0]).toString(),
      maxPrice: Number(values[1]).toString()
    });
  };

  onSelectedMake = selectedMake => {
    this.setState({ selectedMake });
  };
  onConfirmMake = () => {
    this.getModalDropdownList(1);
  };
  onCancelMake = () => {
    this.getModalDropdownList(0);
  };
  getModalDropdownList(type) {
    if (type == 1 || this.state.selectedMake.length > 0) {
      let postData = { make_ids: JSON.stringify(this.state.selectedMake) };
      Agent.CarAPIs.getCarModals(postData).then(response => {
        if (response.data.status) {
          this.setState({
            listModal: [
              { name: "Select Modal", id: "id1", children: response.data.data }
            ]
          });
        }
      });
    } else {
      this.setState({
        listModal: [{ name: "Select Modal", id: "id1", children: [] }]
      });
    }
  }
  onSelectedModal = selectedModal => {
    this.setState({ selectedModal });
  };
  onSelectedType = selectedType => {
    this.setState({ selectedType });
  };
  onSelectedFuelVarient = selectedFuelVarient => {
    this.setState({ selectedFuelVarient });
  };
  onSelectedTransmissionType = selectedTransmissionType => {
    this.setState({ selectedTransmissionType });
  };
  onSelectedYears = selectedYears => {
    this.setState({ selectedYears });
  };
  onSelectedPower = selectedPower => {
    this.setState({ selectedPower });
  };
  onSelectedDoors = selectedDoors => {
    this.setState({ selectedDoors });
  };
  onSelectedColors = selectedColors => {
    this.setState({ selectedColors });
  };
  onSelectedOwners = selectedOwners => {
    this.setState({ selectedOwners });
  };
  onSelectedParking = selectedParking => {
    this.setState({ selectedParking });
  };

  /* -----  End Functions :: set filterd values by id ----- */

  /* -----  Start Functions :: set filterd values by name ----- */
  onSelectedObjMake = obj => {
    this.setFilterdData("Make", obj);
  };
  onSelectedObjModal = obj => {
    this.setFilterdData("Model", obj);
  };
  onSelectedObjType = obj => {
    this.setFilterdData("Category", obj);
  };
  onSelectedObjFuelVarient = obj => {
    this.setFilterdData("FuelType", obj);
  };
  onSelectedObjTransmissionType = obj => {
    this.setFilterdData("TransmissionType", obj);
  };
  onSelectedObjYears = obj => {
    this.setFilterdData("Year", obj);
  };
  onSelectedObjPower = obj => {
    this.setFilterdData("Power", obj);
  };
  onSelectedObjDoors = obj => {
    this.setFilterdData("Door", obj);
  };
  onSelectedObjColors = obj => {
    this.setFilterdData("Color", obj);
  };
  onSelectedObjOwners = obj => {
    this.setFilterdData("Owners", obj);
  };
  onSelectedObjParking = obj => {
    this.setFilterdData("Parking", obj);
  };

  setFilterdData(type, obj) {
    let filterSelectedData = [];
    let dataArr = [];
    if (Helper.objLength(obj) > 0) {
      obj.map(function(v, k) {
        dataArr.push(v.name);
      });
    }
    if (type === "Make")
      filterSelectedData = { keyName: type, value: dataArr.join(", ") };
    else if (type === "Model")
      filterSelectedData = { keyName: type, value: dataArr.join(", ") };
    else if (type === "Category")
      filterSelectedData = { keyName: type, value: dataArr.join(", ") };
    else if (type === "FuelType")
      filterSelectedData = { keyName: "Fuel Type", value: dataArr.join(", ") };
    else if (type === "Year")
      filterSelectedData = { keyName: type, value: dataArr.join(", ") };
    else if (type === "Power")
      filterSelectedData = { keyName: type, value: dataArr.join(", ") };
    else if (type === "Door")
      filterSelectedData = { keyName: type, value: dataArr.join(", ") };
    else if (type === "Color")
      filterSelectedData = { keyName: type, value: dataArr.join(", ") };
    else if (type === "Owners")
      filterSelectedData = { keyName: type, value: dataArr.join(", ") };
    else if (type === "Parking")
      filterSelectedData = { keyName: type, value: dataArr.join(", ") };
    else if (type === "TransmissionType")
      filterSelectedData = {
        keyName: "Transmission Type",
        value: dataArr.join(", ")
      };
    this.setState({ ["filterSelectedData" + type]: filterSelectedData });
  }

  /* -----  End Functions :: set filterd values by name ----- */
  render() {
    return (
      <View style={CarFilterStyle.mainView}>
        <ScrollView>
          <View style={CarFilterStyle.selectView}>
            <View style={CarFilterStyle.selectSubView}>
              <SectionedMultiSelect
                hideSearch
                showCancelButton
                showChips={false}
                items={this.state.listMake}
                uniqueKey="id"
                subKey="children"
                selectText="Make"
                showDropDowns={false}
                readOnlyHeadings={true}
                onSelectedItemsChange={this.onSelectedMake}
                onConfirm={this.onConfirmMake}
                onCancel={this.onCancelMake}
                selectedItems={this.state.selectedMake}
                onSelectedItemObjectsChange={this.onSelectedObjMake}
                expandDropDowns={true}
                styles={{
                  selectToggle: [
                    CarFilterStyle.selectToggle,
                    { marginRight: 5 }
                  ],
                  button: {
                    backgroundColor: kThemeColor,
                    height: 40
                  },
                  cancelButton: {
                    height: 40
                  }
                }}
              />
            </View>

            <View style={CarFilterStyle.selectSubView}>
              <SectionedMultiSelect
                hideSearch
                showCancelButton
                showChips={false}
                items={this.state.listModal}
                uniqueKey="id"
                subKey="children"
                selectText="Modal"
                showDropDowns={false}
                readOnlyHeadings={true}
                onSelectedItemsChange={this.onSelectedModal}
                selectedItems={this.state.selectedModal}
                onSelectedItemObjectsChange={this.onSelectedObjModal}
                expandDropDowns={true}
                styles={{
                  selectToggle: [CarFilterStyle.selectToggle, { marginLeft: 5 }]
                }}
              />
            </View>
          </View>

          <View style={CarFilterStyle.selectView}>
            <View style={CarFilterStyle.selectSubView}>
              <SectionedMultiSelect
                hideSearch
                showCancelButton
                showChips={false}
                items={this.state.listType}
                uniqueKey="id"
                subKey="children"
                selectText="Type"
                showDropDowns={false}
                readOnlyHeadings={true}
                onSelectedItemsChange={this.onSelectedType}
                selectedItems={this.state.selectedType}
                onSelectedItemObjectsChange={this.onSelectedObjType}
                expandDropDowns={true}
                styles={{
                  selectToggle: [
                    CarFilterStyle.selectToggle,
                    { marginRight: 5 }
                  ]
                }}
              />
            </View>

            <View style={CarFilterStyle.selectSubView}>
              <SectionedMultiSelect
                hideSearch
                showCancelButton
                showChips={false}
                items={this.state.listFuelVarient}
                uniqueKey="id"
                subKey="children"
                selectText="Fuel Varient"
                showDropDowns={false}
                readOnlyHeadings={true}
                onSelectedItemsChange={this.onSelectedFuelVarient}
                selectedItems={this.state.selectedFuelVarient}
                onSelectedItemObjectsChange={this.onSelectedObjFuelVarient}
                expandDropDowns={true}
                styles={{
                  selectToggle: [CarFilterStyle.selectToggle, { marginLeft: 5 }]
                }}
              />
            </View>
          </View>

          <Text
            style={CarFilterStyle.serachButtonTextStyle}
            onPress={this.advanceSearch}
          >
            Advance Search
          </Text>
          {this.state.is_advance_search && (
            <View>
              <View style={CarFilterStyle.selectView}>
                <View style={CarFilterStyle.selectSubView}>
                  <SectionedMultiSelect
                    hideSearch
                    showCancelButton
                    showChips={false}
                    items={this.state.listTransmissionType}
                    uniqueKey="id"
                    subKey="children"
                    selectText="Transmission Type"
                    showDropDowns={false}
                    readOnlyHeadings={true}
                    onSelectedItemsChange={this.onSelectedTransmissionType}
                    selectedItems={this.state.selectedTransmissionType}
                    onSelectedItemObjectsChange={
                      this.onSelectedObjTransmissionType
                    }
                    expandDropDowns={true}
                    styles={{
                      selectToggle: [CarFilterStyle.selectToggle]
                    }}
                  />
                </View>
              </View>

              <View style={CarFilterStyle.selectView}>
                <View style={CarFilterStyle.selectSubView}>
                  <TextInput
                    autoCapitalize={"none"}
                    autoCorrect={false}
                    keyboardType="numeric"
                    style={[CarFilterStyle.selectToggle, { marginRight: 5 }]}
                    placeholder="Min Price"
                    underlineColorAndroid={"transparent"}
                    value={this.state.minPrice}
                    onChangeText={price => {
                      this.setState({
                        minPrice: price
                      });
                    }}
                  />
                </View>
                <View style={CarFilterStyle.selectSubView}>
                  <TextInput
                    autoCapitalize={"none"}
                    autoCorrect={false}
                    keyboardType="numeric"
                    style={[CarFilterStyle.selectToggle, { marginLeft: 5 }]}
                    placeholder="Max Price"
                    underlineColorAndroid={"transparent"}
                    value={this.state.maxPrice}
                    onChangeText={price => {
                      this.setState({
                        maxPrice: price
                      });
                    }}
                  />
                </View>
              </View>
              <View
                style={{
                  flex: 1,
                  marginLeft: 15,
                  marginRight: 20
                }}
              >
                <MultiSlider
                  values={[
                    this.state.sliderInitialVals[0],
                    this.state.sliderInitialVals[1]
                  ]}
                  sliderLength={width - 38}
                  onValuesChange={this.multiSliderValuesChange}
                  min={this.state.sliderInitialVals[0]}
                  max={this.state.sliderInitialVals[1]}
                  step={100}
                  touchDimensions={{
                    height: 60,
                    width: 30,
                    borderRadius: 15,
                    slipDisplacement: 30
                  }}
                  containerStyle={{ width: 30 }}
                />
              </View>
              <View style={CarFilterStyle.selectView}>
                <View style={CarFilterStyle.selectSubView}>
                  <SectionedMultiSelect
                    hideSearch
                    showCancelButton
                    showChips={false}
                    items={this.state.listYears}
                    uniqueKey="id"
                    subKey="children"
                    selectText="Year"
                    showDropDowns={false}
                    readOnlyHeadings={true}
                    onSelectedItemsChange={this.onSelectedYears}
                    selectedItems={this.state.selectedYears}
                    onSelectedItemObjectsChange={this.onSelectedObjYears}
                    expandDropDowns={true}
                    styles={{
                      selectToggle: [
                        CarFilterStyle.selectToggle,
                        { marginRight: 5 }
                      ]
                    }}
                  />
                </View>

                <View style={CarFilterStyle.selectSubView}>
                  <SectionedMultiSelect
                    hideSearch
                    showCancelButton
                    showChips={false}
                    items={this.state.listPower}
                    uniqueKey="id"
                    subKey="children"
                    selectText="Power"
                    showDropDowns={false}
                    readOnlyHeadings={true}
                    onSelectedItemsChange={this.onSelectedPower}
                    selectedItems={this.state.selectedPower}
                    onSelectedItemObjectsChange={this.onSelectedObjPower}
                    expandDropDowns={true}
                    styles={{
                      selectToggle: [
                        CarFilterStyle.selectToggle,
                        { marginLeft: 5 }
                      ]
                    }}
                  />
                </View>
              </View>

              <View style={CarFilterStyle.selectView}>
                <View style={CarFilterStyle.selectSubView}>
                  <SectionedMultiSelect
                    hideSearch
                    showCancelButton
                    showChips={false}
                    items={this.state.listDoors}
                    uniqueKey="id"
                    subKey="children"
                    selectText="Door"
                    showDropDowns={false}
                    readOnlyHeadings={true}
                    onSelectedItemsChange={this.onSelectedDoors}
                    selectedItems={this.state.selectedDoors}
                    onSelectedItemObjectsChange={this.onSelectedObjDoors}
                    expandDropDowns={true}
                    styles={{
                      selectToggle: [
                        CarFilterStyle.selectToggle,
                        { marginRight: 5 }
                      ]
                    }}
                  />
                </View>

                <View style={CarFilterStyle.selectSubView}>
                  <SectionedMultiSelect
                    hideSearch
                    showCancelButton
                    showChips={false}
                    items={this.state.listColors}
                    uniqueKey="id"
                    subKey="children"
                    selectText="Color"
                    showDropDowns={false}
                    readOnlyHeadings={true}
                    onSelectedItemsChange={this.onSelectedColors}
                    selectedItems={this.state.selectedColors}
                    onSelectedItemObjectsChange={this.onSelectedObjColors}
                    expandDropDowns={true}
                    styles={{
                      selectToggle: [
                        CarFilterStyle.selectToggle,
                        { marginLeft: 5 }
                      ]
                    }}
                  />
                </View>
              </View>

              <View style={CarFilterStyle.selectView}>
                <View style={CarFilterStyle.selectSubView}>
                  <SectionedMultiSelect
                    hideSearch
                    showCancelButton
                    showChips={false}
                    items={this.state.listOwners}
                    uniqueKey="id"
                    subKey="children"
                    selectText="Owner"
                    showDropDowns={false}
                    readOnlyHeadings={true}
                    onSelectedItemsChange={this.onSelectedOwners}
                    selectedItems={this.state.selectedOwners}
                    onSelectedItemObjectsChange={this.onSelectedObjOwners}
                    expandDropDowns={true}
                    styles={{
                      selectToggle: [
                        CarFilterStyle.selectToggle,
                        { marginRight: 5 }
                      ]
                    }}
                  />
                </View>

                <View style={CarFilterStyle.selectSubView}>
                  <SectionedMultiSelect
                    hideSearch
                    showCancelButton
                    showChips={false}
                    items={this.state.listParking}
                    uniqueKey="id"
                    subKey="children"
                    selectText="Parking"
                    showDropDowns={false}
                    readOnlyHeadings={true}
                    onSelectedItemsChange={this.onSelectedParking}
                    selectedItems={this.state.selectedParking}
                    onSelectedItemObjectsChange={this.onSelectedObjParking}
                    expandDropDowns={true}
                    styles={{
                      selectToggle: [
                        CarFilterStyle.selectToggle,
                        { marginLeft: 5 }
                      ]
                    }}
                  />
                </View>
              </View>
            </View>
          )}

          <View style={CarFilterStyle.buttonView}>
            <TouchableHighlight
              style={CarFilterStyle.buttonStyle}
              underlayColor="transparent"
              onPress={this.handleSearch}
            >
              <Text style={CarFilterStyle.serachButtonTextStyle}> Search </Text>
            </TouchableHighlight>
          </View>
        </ScrollView>
        <DropdownAlert
          ref={ref => (this.dropdown = ref)}
          closeInterval={1500}
        />
        <LoaderClass
          onRef={ref => {
            this.loaderclass = ref;
          }}
        />
      </View>
    );
  }
}
