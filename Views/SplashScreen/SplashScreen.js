import React, { Component } from "react";
import { View, TextInput, TouchableHighlight, Text, Image } from "react-native";
import { styles, SplashScreenStyle } from "../../configs/Styles";
import PushNotification from "./../PartialComponents/PushNotification";
export default class SplashScreen extends Component {
  render() {
    return (
      <View style={SplashScreenStyle.mainView}>
        <Text>{this.props.state.car_details}</Text>
        <Image
          style={SplashScreenStyle.image}
          source={require("../../assets/Images/splash_logo.png")}
        />
        <PushNotification {...this.props} />
      </View>
    );
  }
}
