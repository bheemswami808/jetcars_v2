import React, { Component } from "react";
import {
  View,
  TextInput,
  TouchableHighlight,
  Text,
  Image,
  Platform,
  BackHandler,
  KeyboardAvoidingView
} from "react-native";
import { styles, LoginViewStyle } from "../../configs/Styles";
import PushNotification from "./../PartialComponents/PushNotification";
import Agent from "../../configs/Agent";
import DropdownAlert from "react-native-dropdownalert";
import Store from "react-native-simple-store"; //local storage
import LoaderClass from "../../configs/LoaderClass";
import { NavigationActions, StackActions } from "react-navigation";

export default class LoginView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userEmail: "",
      userPassword: "password",
      userDevice: "iOS",
      userDeviceToken: ""
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentDidMount() {
    global.isViewOpen = 1;
    BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
  }
  onBackPress = () => {
    //BackHandler.exitApp();
    if (global.isViewOpen == 0) {
      //global.isViewOpen = 1;
    } else {
      BackHandler.exitApp();
    }
    const resetAction = StackActions.reset({
      index: 0,
      key: "ds",
      actions: [NavigationActions.navigate({ routeName: "DashboardScreen" })],
      actions: [NavigationActions.navigate({ routeName: "Login" })]
    });
    this.props.navigation.dispatch(resetAction);
  };
  handleSubmit() {
    if (this.state.userEmail == "" || this.state.userPassword == "") {
      if (this.state.userEmail == "") {
        this.dropdown.alertWithType("warn", "Warning", "Enter user email");
      } else {
        this.dropdown.alertWithType("warn", "Warning", "Enter user password");
      }
    } else {
      if (Platform.OS == "android") {
        this.setState({
          userDevice: "Android",
          userDeviceToken: global.deviceToken
        });
      }
      this.loaderclass.loader(true);
      let user_data = {
        email: this.state.userEmail,
        password: this.state.userPassword,
        //email: "user@gmail.com",
        //password: "password",
        device_token: global.deviceToken,
        device_type: this.state.userDevice
      };

      Agent.UserApi.loginUser(user_data).then(response => {
        this.loaderclass.loader(false);
        global.userDetails = response.data.data;
        if (response.data.status) {
          if (global.userDetails.pass_updated == 1) {
            Store.update("auth", global.userDetails);
            this.props.navigation.navigate("Dashboard");
          } else {
            this.props.navigation.navigate("ResetPassword");
          }
        } else {
          this.dropdown.alertWithType("error", "Error", response.data.msg);
        }
      });
    }
  }
  render() {
    const { navigate } = this.props.navigation;
    return (
      <KeyboardAvoidingView
        style={LoginViewStyle.mainView}
        behavior="padding"
        enabled
      >
        <PushNotification {...this.props} />
        <Image
          style={LoginViewStyle.footerImage}
          source={require("../../assets/Images/footer_bg2.png")}
        />

        {/* header view */}
        <View style={LoginViewStyle.topLogo}>
          <View style={LoginViewStyle.mainPageView}>
            <Image
              source={require("../../assets/Images/splash_logo.png")}
              style={styles.headerImage}
            />
          </View>
        </View>

        {/* textinput views */}
        <View style={LoginViewStyle.textInputsView}>
          <TextInput
            style={[styles.textinput, styles.topRadius]}
            autoCorrect={false}
            autoCapitalize={"none"}
            keyboardType="email-address"
            ReturnKeyType="next"
            placeholder="Username/Email"
            underlineColorAndroid={"transparent"}
            onChangeText={text => {
              this.setState({
                userEmail: text
              });
            }}
          />
          <TextInput
            autoCapitalize={"none"}
            autoCorrect={false}
            style={[styles.textinput, styles.bottomRadius, styles.topBorder]}
            placeholder="Password"
            secureTextEntry={true}
            underlineColorAndroid={"transparent"}
            onChangeText={text => {
              this.setState({
                userPassword: text
              });
            }}
          />
        </View>

        {/* Login button view  */}
        <View style={LoginViewStyle.footerButtonView}>
          {/* login button */}
          <TouchableHighlight
            style={styles.buttonStyle}
            underlayColor="transparent"
            onPress={this.handleSubmit}
          >
            <Text style={styles.footerButtonTextStyle}> Login </Text>
          </TouchableHighlight>
          {/* forgot password */}
          <TouchableHighlight
            onPress={() => {
              global.isViewOpen = 0;
              navigate("ForgotEMail");
            }}
            underlayColor="transparent"
          >
            <Text style={styles.anchorTextStyle}>Forgot Password ? </Text>
          </TouchableHighlight>
        </View>
        <DropdownAlert
          ref={ref => (this.dropdown = ref)}
          closeInterval={1500}
        />
        <LoaderClass
          onRef={ref => {
            this.loaderclass = ref;
          }}
        />
      </KeyboardAvoidingView>
    );
  }
}
