import React, { Component } from "react";
import { View, TextInput, TouchableHighlight, Text, Image } from "react-native";
import { styles, LoginViewStyle } from "../../configs/Styles";

import Agent from "../../configs/Agent";
import DropdownAlert from "react-native-dropdownalert";

export default class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newPassword: "",
      confirmPassword: ""
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit() {
    if (
      this.state.newPassword == "" ||
      this.state.newPassword.length < 6 ||
      this.state.confirmPassword == "" ||
      this.state.newPassword != this.state.confirmPassword
    ) {
      if (this.state.newPassword == "") {
        this.dropdown.alertWithType("warn", "Warning", "Enter new password");
      } else if (this.state.confirmPassword == "") {
        this.dropdown.alertWithType(
          "warn",
          "Warning",
          "Enter confirm password"
        );
      } else if (this.state.newPassword.length < 6) {
        this.dropdown.alertWithType(
          "warn",
          "Warning",
          "New Password should be minimum 6 character long"
        );
      } else {
        this.dropdown.alertWithType(
          "warn",
          "Warning",
          "New password and confirmation password do not match."
        );
      }
    } else {
      let user_data = {
        user_id: global.userDetails.id,
        old_password: this.state.newPassword,
        new_password: this.state.confirmPassword
      };
      Agent.UserApi.updatePassword(user_data).then(response => {
        if (response.data.status) {
          this.props.navigation.navigate("Dashboard");
        } else {
          this.dropdown.alertWithType("error", "Error", response.data.msg);
        }
      });
    }
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={LoginViewStyle.mainView}>
        <Image
          style={LoginViewStyle.footerImage}
          source={require("../../assets/Images/footer_bg2.png")}
        />
        {/* header view */}
        <View style={LoginViewStyle.topLogo}>
          <View style={LoginViewStyle.mainPageView}>
            <Image
              source={require("../../assets/Images/splash_logo.png")}
              style={styles.headerImage}
            />
          </View>
        </View>

        {/* textinput views */}
        <View style={LoginViewStyle.textInputsView}>
          <TextInput
            style={[styles.textinput, styles.topRadius]}
            autoCorrect={false}
            autoCapitalize={"none"}
            secureTextEntry={true}
            placeholder="New Password"
            underlineColorAndroid={"transparent"}
            onChangeText={text => {
              this.setState({
                newPassword: text
              });
            }}
          />
          <TextInput
            style={[styles.textinput, styles.bottomRadius, styles.topBorder]}
            placeholder="Confirm Password"
            secureTextEntry={true}
            autoCapitalize={"none"}
            autoCorrect={false}
            underlineColorAndroid={"transparent"}
            onChangeText={text => {
              this.setState({
                confirmPassword: text
              });
            }}
          />
        </View>

        {/* next button view  */}
        <View style={LoginViewStyle.footerButtonView}>
          {/* Next Button */}
          <TouchableHighlight
            style={styles.buttonStyle}
            onPress={this.handleSubmit}
          >
            <Text style={styles.footerButtonTextStyle}> Next </Text>
          </TouchableHighlight>

          {/* skip Button */}
          <TouchableHighlight
            style={styles.footerButtonTextStyle}
            onPress={() => navigate("Dashboard")}
          >
            <Text style={styles.anchorTextStyle}> Skip ></Text>
          </TouchableHighlight>
        </View>
        <DropdownAlert
          ref={ref => (this.dropdown = ref)}
          closeInterval={1500}
        />
      </View>
    );
  }
}
