import React, { Component } from "react";
import {
  View,
  TextInput,
  TouchableHighlight,
  Text,
  Image,
  KeyboardAvoidingView
} from "react-native";
import { styles, ForgotViewStyle, LoginViewStyle } from "../../configs/Styles";

import Agent from "../../configs/Agent";
import DropdownAlert from "react-native-dropdownalert";
import LoaderClass from "../../configs/LoaderClass";
import {} from "react-native-keyboard-aware-scroll-view";
export default class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userEmail: "",
      error: ""
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit() {
    if (this.state.userEmail == "") {
      this.dropdown.alertWithType("warn", "Warning", "Enter user email");
    } else {
      this.loaderclass.loader(true);
      let user_data = {
        email: this.state.userEmail
      };
      Agent.UserApi.forgotPassword(user_data).then(response => {
        this.loaderclass.loader(false);
        if (response.data.status) {
          this.props.navigation.navigate("Login");
        } else {
          this.dropdown.alertWithType("error", "Error", response.data.msg);
        }
      });
    }
  }

  render() {
    const { navigate } = this.props.navigation;

    return (
      <KeyboardAvoidingView
        style={ForgotViewStyle.mainView}
        behavior="padding"
        enabled
      >
        <Image
          style={LoginViewStyle.footerImage}
          source={require("../../assets/Images/footer_bg2.png")}
        />
        {/* header view */}

        <View style={LoginViewStyle.topLogo}>
          <View style={LoginViewStyle.mainPageView}>
            <Image
              source={require("../../assets/Images/splash_logo.png")}
              style={styles.headerImage}
            />
          </View>
        </View>

        {/* textinput views */}
        <View style={ForgotViewStyle.textInputsView}>
          <TextInput
            style={[styles.textinput, styles.topRadius, styles.bottomRadius]}
            autoCorrect={false}
            autoCapitalize={"none"}
            keyboardType="email-address"
            placeholder="Enter your email"
            underlineColorAndroid={"transparent"}
            onChangeText={text => {
              this.setState({
                userEmail: text
              });
            }}
          />
        </View>

        {/* Login button view  */}
        <View style={ForgotViewStyle.footerButtonView}>
          {/* login button */}
          <TouchableHighlight
            style={styles.buttonStyle}
            onPress={this.handleSubmit}
          >
            <Text style={styles.footerButtonTextStyle}> Proceed </Text>
          </TouchableHighlight>
        </View>

        <DropdownAlert
          ref={ref => (this.dropdown = ref)}
          closeInterval={1500}
        />
        <LoaderClass
          onRef={ref => {
            this.loaderclass = ref;
          }}
        />
      </KeyboardAvoidingView>
    );
  }
}
