import React, { Component } from "react";
import {
  View,
  TextInput,
  TouchableHighlight,
  Text,
  FlatList
} from "react-native";
import { styles, NotificationStyle } from "../../configs/Styles";
import Agent from "../../configs/Agent";
import DropdownAlert from "react-native-dropdownalert";
import LoaderClass from "../../configs/LoaderClass";
export default class Notification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      NotificationData: []
    };

    this.detailTrigger = this.detailTrigger.bind(this);
  }

  detailTrigger(notiData) {
    this.loaderclass.loader(true);
    let user_data = {
      vehicle_id: notiData.vehicle_id
    };

    Agent.NotificationApi.VehicleDetail(user_data).then(response => {
      if (response.data.status) {
        let resData = response.data.data;
        this.props.navigation.navigate("CarDetails", { resData });
      } else {
        this.dropdown.alertWithType("error", "", "Data not found");
      }
      this.loaderclass.loader(false);
    });
  }

  componentDidMount() {
    global.isViewOpen = 0;
    let user_data = {
      user_id: global.userDetails.id
    };
    this.loaderclass.loader(true);
    Agent.NotificationApi.notificationList(user_data).then(response => {
      if (response.data.status) {
        this.setState({
          NotificationData: response.data.data
        });
      } else {
        this.dropdown.alertWithType("error", "", "Data not found");
      }
      this.loaderclass.loader(false);
    });
  }
  render() {
    return (
      <View style={NotificationStyle.mainView}>
        <FlatList
          style={NotificationStyle.flatList}
          data={this.state.NotificationData}
          renderItem={({ item }) => (
            <TouchableHighlight
              style={{
                padding: 10,
                borderBottomWidth: 1,
                borderBottomColor: "grey"
              }}
              onPress={() => {
                this.detailTrigger(item);
              }}
              underlayColor="transparent"
            >
              <View>
                <Text style={[styles.blackText]}>{item.message}</Text>
                <Text
                  style={[styles.blackText, { fontSize: 12, color: "grey" }]}
                >
                  {item.date}
                </Text>
              </View>
            </TouchableHighlight>
          )}
        />
        <DropdownAlert
          ref={ref => (this.dropdown = ref)}
          closeInterval={1500}
        />
        <LoaderClass
          onRef={ref => {
            this.loaderclass = ref;
          }}
        />
      </View>
    );
  }
}
