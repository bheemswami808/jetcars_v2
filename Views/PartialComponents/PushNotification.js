import React, { Component } from "react";
import { Platform, Text } from "react-native";
import firebase from "react-native-firebase";
import Agent from "./../../configs/Agent";
import type { Notification, NotificationOpen } from "react-native-firebase";

export default class PushNotification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      car_details: false,
      splashEnd: false,
      token: ""
    };
    this.redirectCarDetailsPage = this.redirectCarDetailsPage.bind(this);
  }
  async componentDidMount() {
    let redirectCarDetailsPage = this.redirectCarDetailsPage;
    const notificationOpen: NotificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    if (notificationOpen) {
      const notification: Notification = notificationOpen.notification;
      if ((notification.data.action = "car_details")) {
        //this.props.navigation.navigate("Dashboard");
        this.redirectCarDetailsPage(notification);
      }
    }
    this.notificationListener = firebase
      .notifications()
      .onNotification((notification: Notification) => {
        notification.android
          .setChannelId("jetCar-channel")
          .setSound("default")
          .android.setSmallIcon("ic_launcher");
        firebase.notifications().displayNotification(notification);
      });

    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened((notificationOpen: NotificationOpen) => {
        const notification: Notification = notificationOpen.notification; // Get information about the notification that was opened
        if ((notification.data.action = "car_details")) {
          //this.props.navigation.navigate("Dashboard");
          this.redirectCarDetailsPage(notification);
        }

        firebase
          .notifications()
          .removeDeliveredNotification(notification.notificationId);
      });
    firebase
      .messaging()
      .getToken()
      .then(token => {
        if (token) {
          global.deviceToken = token;
        }
      });
    firebase.messaging().onTokenRefresh(token => {
      if (token) {
        global.deviceToken = token;
      }
    });
  }
  redirectCarDetailsPage(notification) {
    if (notification.data.id > 0) {
      let postData = { vehicle_id: notification.data.id };
      Agent.NotificationApi.VehicleDetail(postData).then(response => {
        if (response.data.status) {
          let resData = response.data.data;
          this.props.navigation.navigate("CarDetails", { resData });
        }
      });
    }
  }
  render() {
    return null;
  }
}
