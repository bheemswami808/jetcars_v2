import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableHighlight,
  BackHandler
} from "react-native";
import { styles, CarListStyle } from "../../configs/Styles";
import DropdownAlert from "react-native-dropdownalert";
import Agent from "../../configs/Agent";
import { NavigationActions, StackActions } from "react-navigation";
export default class CarList extends Component {
  constructor(props) {
    super(props);
    this.detailTrigger = this.detailTrigger.bind(this);
  }

  detailTrigger(item) {
    this.props.tapExitUpdate();
    global.isViewOpen = 0;
    this.props.navigation.navigate("CarDetails", { ...this.props.item });
  }

  render() {
    return (
      <TouchableHighlight
        onPress={this.detailTrigger}
        underlayColor="transparent"
      >
        <View style={CarListStyle.card}>
          {/* title view */}
          <View style={CarListStyle.titleBarParent}>
            <View style={CarListStyle.title}>
              <Text style={[styles.whiteText, CarListStyle.titleText]}>
                {this.props.item.car_name}
              </Text>
            </View>
            <View style={CarListStyle.comment}>
              <Image
                source={require("../../assets/Images/chat.png")}
                style={CarListStyle.chatIcon}
              />
              <Text style={styles.whiteText}>
                {this.props.item.comment_count}
              </Text>
            </View>
          </View>
          {/* image slider view */}
          <View style={CarListStyle.sliderParent}>
            {this.props.item.photo_array[0] !== undefined ? (
              <Image
                source={{
                  uri:
                    this.props.item.image_path + this.props.item.photo_array[0]
                }}
                style={CarListStyle.imageSlide}
              />
            ) : (
              <Image
                source={require("../../assets/Images/splash_img.png")}
                style={CarListStyle.imageSlide}
              />
            )}
          </View>

          <View style={CarListStyle.detailParent}>
            <View style={styles.directionRow}>
              <View style={styles.alignLeft}>
                <Text style={[styles.blackText]}>
                  Price:
                  <Text style={styles.headings}>
                    {" "}
                    € {this.props.item.price}
                  </Text>
                </Text>
              </View>
              <View style={styles.alignRight}>
                <Text style={[styles.blackText]}>
                  Driven: {this.props.item.driven} Km
                </Text>
              </View>
            </View>

            <View style={styles.directionRow}>
              <View style={styles.alignLeft}>
                <Text style={[styles.blackText]}>
                  Reg. number: {this.props.item.registration}
                </Text>
              </View>
              <View style={styles.alignRight}>
                <Text style={[styles.blackText]}>
                  Engine Size: {this.props.item.engine_size} Ltr
                </Text>
              </View>
            </View>
            <View style={styles.directionRow}>
              <View style={styles.alignLeft}>
                <Text style={[styles.blackText, styles.headings]}>Details</Text>
              </View>
            </View>

            <View style={styles.directionRow}>
              <View style={[styles.alignLeft]}>
                <Text style={[styles.blackText]} numberOfLines={1}>
                  {this.props.item.other_details}
                </Text>
              </View>
            </View>

            <View style={[styles.alignLeft, { flexDirection: "row" }]}>
              <Image
                source={require("../../assets/Images/calendar.png")}
                style={CarListStyle.calenderIcon}
              />
              <Text style={[styles.blackText, CarListStyle.grayTimeText]}>
                {this.props.item.created_at}
              </Text>
            </View>
          </View>

          <DropdownAlert
            ref={ref => (this.dropdown = ref)}
            closeInterval={1500}
          />
        </View>
      </TouchableHighlight>
    );
  }
}
