/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import { View, Text, Image, TouchableHighlight } from "react-native";
import { UserCommentsStyle } from "../../configs/Styles";

export default class userComments extends Component {
  constructor(props) {
    super(props);
    this.detailTrigger = this.detailTrigger.bind(this);
  }
  detailTrigger() {
    this.props.navigation.navigate("CarDetails");
  }
  render() {
    return (
      <View style={UserCommentsStyle.card}>
        {this.props.item.user_id != global.userDetails.id ? (
          <View>
            <Text style={UserCommentsStyle.userNameText}>
              {this.props.item.user_name} :
            </Text>

            <View style={UserCommentsStyle.Commentscard}>
              <Text style={UserCommentsStyle.userCommentText}>
                {this.props.item.comment_text}
              </Text>
            </View>

            <View
              style={[
                UserCommentsStyle.Commentscard,
                { backgroundColor: "white" }
              ]}
            >
              <Text style={UserCommentsStyle.userCommentDateText}>
                {this.props.item.date}
              </Text>
            </View>
          </View>
        ) : (
          <View>
            <Text
              style={[
                UserCommentsStyle.userNameText,
                { marginLeft: 40, marginRight: 0, textAlign: "right" }
              ]}
            >
              {global.userDetails.name} :
            </Text>

            <View
              style={[
                UserCommentsStyle.Commentscard,
                {
                  alignSelf: "flex-end",
                  marginRight: 5,
                  marginLeft: 40
                }
              ]}
            >
              <Text style={UserCommentsStyle.userCommentText}>
                {this.props.item.comment_text}
              </Text>
            </View>

            <View
              style={[
                UserCommentsStyle.Commentscard,
                {
                  alignSelf: "flex-end",
                  marginRight: 5,
                  marginLeft: 40,
                  backgroundColor: "white"
                }
              ]}
            >
              <Text style={UserCommentsStyle.userCommentDateText}>
                {this.props.item.date}
              </Text>
            </View>
          </View>
        )}
      </View>
    );
  }
}
