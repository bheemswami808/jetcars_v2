import React, { Component } from "react";
import {
  View,
  ScrollView,
  Text,
  Image,
  FlatList,
  TextInput,
  TouchableHighlight,
  TouchableOpacity,
  Modal,
  Keyboard
} from "react-native";
import { styles, CarDetailsStyle, kThemeColor } from "../../configs/Styles";
import UserComment from "../PartialComponents/userComments";
import Swiper from "react-native-swiper";
import Agent from "../../configs/Agent";
import Helper from "../../configs/Helper";
import DropdownAlert from "react-native-dropdownalert";
import { SinglePickerMaterialDialog } from "react-native-material-dialog";
//import Icon from "react-native-vector-icons/FontAwesome";
import LoaderClass from "../../configs/LoaderClass";
export default class CarDetailsView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startswiper: false,
      carData: {},
      userCommentsText: "",
      commentsData: [],
      showMoreComments: false,
      detailsData: [],
      showStatusPopup: false,
      route: "In Stock",
      selectedStatus: {
        value: 0,
        label: "In Stock"
      },
      showStatusText: false,
      statusComment: "",
      soldPrice: "",
      page_step: 5
    };
    this._renderSwiper = this._renderSwiper.bind(this);
    this.sendUserComments = this.sendUserComments.bind(this);
    this.loadMoreComments = this.loadMoreComments.bind(this);
    this.setSelectedStatus = this.setSelectedStatus.bind(this);
  }
  componentDidMount() {
    global.isViewOpen = 0;
    setTimeout(() => {
      this.getCarCommentsList();
      this.setState({
        startswiper: true
      });
    }, 500);

    let carData = {};
    if (Helper.objLength(this.props.navigation.state.params.resData) > 0) {
      carData = this.props.navigation.state.params.resData;
    } else {
      carData = this.props.navigation.state.params;
    }

    this.setState({ carData });
    this.setSelectedStatus();
  }
  setSelectedStatus() {
    this.setState({
      selectedStatus:
        this.props.navigation.state.params.vehicle_status === "In Stock"
          ? {
              value: 0,
              label: "In Stock"
            }
          : {
              value: 1,
              label: "Reserved"
            }
    });
  }
  getCarCommentsList() {
    let user_data = { vehicle_id: this.state.carData.vehicle_id };
    Agent.CarAPIs.getCarDetails(user_data).then(response => {
      if (response.data.status) {
        this.setState({
          detailsData: response.data.accessories,
          allComments: response.data.comments,
          commentsData: Helper.commentPaginator(
            response.data.comments,
            0,
            this.state.page_step
          )
        });
      } else {
        this.dropdown.alertWithType("error", "Error", response.data.msg);
      }
    });
  }

  loadMoreComments() {
    let page_step = this.state.page_step + 5;
    let commentsData = Helper.commentPaginator(
      this.state.allComments,
      0,
      page_step
    );
    this.setState({ page_step, commentsData });
  }
  _renderSwiper() {
    const path = this.state.carData.image_path;
    let images = this.state.carData.photo_array.map(function(val, idx) {
      return (
        <View key={idx}>
          <Image
            style={{
              width: "100%",
              height: "100%",
              resizeMode: "cover"
            }}
            source={{
              uri: path + val
            }}
          />
        </View>
      );
    });
    if (images.length > 0) {
      return (
        <Swiper
          dotColor={kThemeColor}
          autoplay
          activeDotColor="#FFF"
          autoplayTimeout={5}
        >
          {images}
        </Swiper>
      );
    } else {
      return (
        <Swiper
          dotColor={kThemeColor}
          autoplay
          activeDotColor="#FFF"
          //autoplayTimeout={5}
        >
          <View>
            <Image
              style={{
                width: "100%",
                height: "100%",
                resizeMode: "cover"
              }}
              source={require("../../assets/Images/splash_img.png")}
            />
          </View>
        </Swiper>
      );
    }
  }

  sendUserComments() {
    if (this.state.userCommentsText == "") {
      this.dropdown.alertWithType("warn", "Warning", "Enter comments");
    } else {
      let user_data = {
        user_id: global.userDetails.id,
        vehicle_id: this.state.carData.vehicle_id,
        comment_text: this.state.userCommentsText
      };
      this.loaderclass.loader(true);
      Agent.CarAPIs.sendUserCarComments(user_data).then(response => {
        Keyboard.dismiss();
        if (response.data.status) {
          this.dropdown.alertWithType(
            "success",
            "success",
            "Comments successfully add"
          );
          this.getCarCommentsList();
          this.setState({
            userCommentsText: ""
          });
        } else {
          this.dropdown.alertWithType("error", "Error", response.data.msg);
        }
        this.loaderclass.loader(false);
      });
    }
  }
  onOkMaterialDialog = result => {
    this.setState(
      {
        showStatusPopup: false,
        selectedStatus: result.selectedItem,
        selectedStatusValue: result.selectedItem.value
      },
      () => {
        if (result.selectedItem.value > 0) {
          this.setState({ showStatusText: true });
        } else {
          this.changeVehicleStatus();
        }
      }
    );
  };

  changeVehicleStatus = () => {
    let statusVal = this.state.selectedStatusValue;
    let postData = {
      sold_by_user: global.userDetails.id,
      vehicle_id: this.state.carData.vehicle_id,
      status: this.state.selectedStatusValue
    };
    if (statusVal > 0) {
      postData.sold_price = this.state.soldPrice;
      postData.comment_text = this.state.statusComment;
    }
    if (this.state.soldPrice === "" && statusVal > 0) {
      this.dropdown.alertWithType("warn", "Warning", "Enter Sold Price");
    } else if (this.state.statusComment === "" && statusVal > 0) {
      this.dropdown.alertWithType("warn", "Warning", "Enter Comment");
    } else {
      Agent.CarAPIs.changeVehicleStatus(postData).then(response => {
        if (response.data.status) {
          if (statusVal > 0) {
            this.getCarCommentsList();
          }
          this.dropdown.alertWithType("success", "Success", response.data.msg);
        } else {
          this.dropdown.alertWithType("error", "Error", response.data.msg);
        }
        this.setState({
          showStatusText: false,
          soldPrice: "",
          statusComment: ""
        });
      });
    }
  };
  render() {
    return (
      <View style={CarDetailsStyle.mainView}>
        <View
          style={{
            position: "absolute",
            left: 0,
            flex: 1,
            height: 64,
            top: 0,
            right: 0,
            backgroundColor: kThemeColor
          }}
        >
          <Text
            style={{
              color: "white",
              fontSize: 16,
              fontFamily: "muli-Regular",
              textAlign: "left",
              paddingVertical: 30,
              marginRight: 110,
              marginLeft: 30
            }}
          >
            {this.state.carData.car_name}
          </Text>

          <TouchableOpacity
            style={{
              position: "absolute",
              left: 2,
              top: 25,
              height: 30,
              justifyContent: "center"
            }}
            onPress={() => this.props.navigation.goBack(null)}
          >
            <Image
              source={require("../../assets/Images/backIcon.png")}
              style={{
                width: 20,
                height: 20,
                marginVertical: 12,
                marginHorizontal: 0.5
              }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              position: "absolute",
              right: 10,
              top: 25,
              height: 30,
              justifyContent: "center"
            }}
            onPress={() => {
              this.setState({
                showStatusPopup: true
              });
            }}
          >
            <Text style={{ color: "white" }}>Change Status</Text>
          </TouchableOpacity>
        </View>

        <ScrollView
          style={[CarDetailsStyle.mainView, { marginTop: 64 }]}
          ref="myScrollView"
        >
          {/* header view */}
          <SinglePickerMaterialDialog
            title={"Car Status :"}
            visible={this.state.showStatusPopup}
            items={["In Stock", "Reserved", "Sold Out"].map((row, index) => ({
              value: index,
              label: row
            }))}
            selectedItem={this.state.selectedStatus}
            onCancel={() => {
              this.setState({ showStatusPopup: false });
            }}
            onOk={this.onOkMaterialDialog}
          />
          <Modal
            animationType="fade"
            transparent={true}
            visible={this.state.showStatusText}
            onRequestClose={() => {}}
            style={styles.blueBackground}
          >
            <View style={{ backgroundColor: "transparent", flex: 1 }}>
              <ScrollView>
                <View style={[CarDetailsStyle.modalStyle]}>
                  <TextInput
                    autoCapitalize={"none"}
                    autoCorrect={false}
                    keyboardType="numeric"
                    style={[
                      styles.textinput,
                      styles.topRadius,
                      { marginTop: 40 }
                    ]}
                    placeholder="Sold Price"
                    underlineColorAndroid={"transparent"}
                    onChangeText={soldPrice => {
                      this.setState({ soldPrice });
                    }}
                  />
                  <TextInput
                    autoCapitalize={"none"}
                    autoCorrect={false}
                    multiline={true}
                    style={[
                      styles.textinput,
                      styles.bottomRadius,
                      styles.topBorder,
                      { height: 150, padding: 10 }
                    ]}
                    placeholder="Comment"
                    underlineColorAndroid={"transparent"}
                    onChangeText={statusComment => {
                      this.setState({ statusComment });
                    }}
                    numberOfLines={10}
                  />
                  <TouchableHighlight
                    style={[
                      styles.buttonStyle,
                      CarDetailsStyle.modalButton,
                      { marginBottom: 2, marginTop: 100 }
                    ]}
                    underlayColor="transparent"
                    onPress={this.changeVehicleStatus}
                  >
                    <Text style={styles.footerButtonTextStyle}> Save </Text>
                  </TouchableHighlight>
                  <TouchableHighlight
                    style={[styles.buttonStyle, CarDetailsStyle.modalButton]}
                    underlayColor="transparent"
                    onPress={() => {
                      this.setState({
                        showStatusText: false
                      });
                      this.setSelectedStatus();
                    }}
                  >
                    <Text style={styles.footerButtonTextStyle}> Cancel </Text>
                  </TouchableHighlight>
                </View>
              </ScrollView>
            </View>
          </Modal>

          <View style={CarDetailsStyle.mainPageView}>
            {this.state.startswiper === true ? this._renderSwiper() : null}
          </View>
          {/* title views */}
          <View style={CarDetailsStyle.carDetailsView}>
            {/* add title  */}
            <View style={CarDetailsStyle.headTitleTexts}>
              <Text
                style={[
                  styles.footerButtonTextStyle,
                  CarDetailsStyle.DetailsTextStyle
                ]}
              >
                {this.state.carData.car_name}
              </Text>
            </View>
            <View style={CarDetailsStyle.lineView} />

            {/* add view for other details */}
            <View style={CarDetailsStyle.mutliItmesView}>
              <Text
                style={[
                  styles.footerButtonTextStyle,
                  CarDetailsStyle.multiTextview
                ]}
              >
                € {this.state.carData.price}
              </Text>
              <Text
                style={[
                  styles.footerButtonTextStyle,
                  CarDetailsStyle.multiTextview,
                  { textAlign: "center" }
                ]}
              >
                {this.state.carData.driven} KM
              </Text>
              <Text
                style={[
                  styles.footerButtonTextStyle,
                  CarDetailsStyle.multiTextview,
                  { textAlign: "right" }
                ]}
              >
                {this.state.carData.registration}
              </Text>
            </View>
            <View style={CarDetailsStyle.lineView} />

            {/* vehical detaails view */}
          </View>

          {/* Vehical Details  */}
          <View style={CarDetailsStyle.carDetailsView}>
            <Text
              style={[
                styles.footerButtonTextStyle,
                CarDetailsStyle.HeaderTextStyle
              ]}
            >
              Vehical Details
            </Text>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Make
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.make}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Modal
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.model}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Color
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.color}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Power
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.power}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Mileage
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.milage}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Numbers of Gears
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.gears}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Door Count
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.door_count}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Seats
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.seats}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Engine Size
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.engine_size}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Cylinders
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.cylinder_count}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Weight
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.weight}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Owners
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.owner_count}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Category
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.category}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Transmission
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.transmission}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Kerb Weight
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.kerb_weight}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Emission Class
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.emission_class}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                CO2 Emission Class
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.co2emission_class}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Fuel Type
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.fuel_type}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Fuel Comsumption
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.fuel_comsumption}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Fuel Tank
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.fuel_tank}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Parking
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.parking}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Vehicle Status
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.vehicle_status}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Supplier
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.supplier}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Owner Name
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.user_name}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Owner Email
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.user_email}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Owner Phone
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.user_phone}
              </Text>
            </View>
            <View style={CarDetailsStyle.VehicalDetailsStyle}>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { fontFamily: "Muli-Bold", color: "black" }
                ]}
              >
                Used/New
              </Text>
              <Text
                style={[
                  CarDetailsStyle.VehicalDetailsInViewTextStyle,
                  { marginLeft: 1 }
                ]}
              >
                {this.state.carData.used_new}
              </Text>
            </View>
            <View style={CarDetailsStyle.carDetailsView} />
            {this.state.detailsData.length > 0 && (
              <Text
                style={[
                  styles.footerButtonTextStyle,
                  CarDetailsStyle.HeaderTextStyle
                ]}
              >
                Features:
              </Text>
            )}

            <View>
              {this.state.detailsData.map((v, k) => {
                return (
                  <View style={CarDetailsStyle.VehicalDetailsStyle}>
                    <Text
                      style={[
                        CarDetailsStyle.VehicalDetailsInViewTextStyle,
                        { fontFamily: "Muli-Bold", color: "black" }
                      ]}
                    >
                      {v.assc_name}
                    </Text>
                    <Text
                      style={[
                        CarDetailsStyle.VehicalDetailsInViewTextStyle,
                        { marginLeft: 1 }
                      ]}
                    >
                      {v.available === "Y" ? (
                        <Text style={{ color: "green" }}>Yes</Text>
                      ) : (
                        <Text style={{ color: "red" }}>No</Text>
                      )}
                    </Text>
                  </View>
                );
              })}
            </View>

            {this.state.commentsData.length > 0 ? (
              <View>
                <Text
                  style={[
                    styles.footerButtonTextStyle,
                    CarDetailsStyle.HeaderTextStyle
                  ]}
                >
                  Comments:
                </Text>
                {this.state.allComments.length !==
                  this.state.commentsData.length && (
                  <TouchableHighlight
                    style={{ backgroundColor: "#fff" }}
                    onPress={this.loadMoreComments}
                  >
                    <Text style={{ fontWeight: "600", textAlign: "center" }}>
                      {/*<Icon name="refresh" size={16} />*/}
                      Load previous comments
                    </Text>
                  </TouchableHighlight>
                )}
                <FlatList
                  style={{ marginBottom: 100 }}
                  data={this.state.commentsData}
                  renderItem={({ item }) => (
                    <UserComment {...this.props} item={item} />
                  )}
                />
              </View>
            ) : (
              <View>
                <Text
                  style={[
                    styles.footerButtonTextStyle,
                    CarDetailsStyle.HeaderTextStyle
                  ]}
                >
                  Comment:
                </Text>
                <Text
                  style={[
                    styles.footerButtonTextStyle,
                    CarDetailsStyle.HeaderTextStyle,
                    { color: "grey", fontSize: 13, fontFamily: "Muli-Regular" }
                  ]}
                >
                  {" "}
                  No Comment Found
                </Text>
              </View>
            )}
          </View>
        </ScrollView>

        <View style={CarDetailsStyle.messageViewStyle}>
          <TextInput
            style={CarDetailsStyle.MessageTextStyle}
            autoCapitalize={"none"}
            autoCorrect={false}
            placeholder="Enter message"
            underlineColorAndroid={"transparent"}
            value={this.state.userCommentsText}
            onChangeText={text => {
              this.setState({
                userCommentsText: text
              });
            }}
          />

          <TouchableHighlight
            style={CarDetailsStyle.sendButtonStyle}
            onPress={this.sendUserComments}
          >
            <Image
              style={CarDetailsStyle.sendButtonImageStyle}
              source={require("../../assets/Images/sendIcon.png")}
            />
          </TouchableHighlight>
        </View>

        <DropdownAlert
          ref={ref => (this.dropdown = ref)}
          closeInterval={1500}
          wrapperStyle={{ zindex: 1 }}
        />
        <LoaderClass
          onRef={ref => {
            this.loaderclass = ref;
          }}
        />
      </View>
    );
  }
}
