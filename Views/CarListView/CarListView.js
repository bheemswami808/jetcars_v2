import React, { Component } from "react";
import {
  View,
  Text,
  FlatList,
  TouchableHighlight,
  TextInput,
  RefreshControl,
  Image,
  BackHandler,
  BackAndroid,
  ToastAndroid,
  TouchableOpacity
} from "react-native";
import { styles, CarDetailsStyle, kThemeColor } from "../../configs/Styles";
import CarList from "../PartialComponents/CarList";
import Agent from "../../configs/Agent";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import DropdownAlert from "react-native-dropdownalert";
import LoaderClass from "../../configs/LoaderClass";
import Menu from "react-native-pop-menu";
import { NavigationActions, StackActions } from "react-navigation";
import PushNotification from "../PartialComponents/PushNotification";
const initialPage = 2;
export default class CarListView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      textInput: "",
      isNotifyView: false,
      filterListData: [],
      notifyFilterItems: {},
      refreshing: false,
      page: initialPage,
      menuVisible: false,
      closeApp: true,
      tapExit: 1
    };
    this.sendNotifyMe = this.sendNotifyMe.bind(this);
    this.onBackPress = this.onBackPress.bind(this);
    this.tapExitUpdate = this.tapExitUpdate.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.navigation.state.params.isViewOpen === "notify") {
      this.setState({
        isNotifyView: true,
        filterListData: nextProps.navigation.state.params.filterPushData,
        notifyFilterItems: nextProps.navigation.state.params.postData
      });
    } else {
      this.setState({
        data: nextProps.navigation.state.params.carListData
      });
    }
  }
  componentWillMount() {
    Agent.CarAPIs.getCarList().then(response => {
      if (response.data.status) {
        this.setState({
          data: response.data.data
        });
        this.loaderclass.loader(false);
      }
    });
    BackAndroid.removeEventListener("hardwareBackPress", this.onBackPress);
  }
  componentDidMount() {
    //redirect carDetails page when open push notification
    if (global.detailsVehicleId > 0) {
      let postData = { vehicle_id: global.detailsVehicleId };
      Agent.NotificationApi.VehicleDetail(postData).then(response => {
        if (response.data.status) {
          let resData = response.data.data;
          this.props.navigation.navigate("CarDetails", { resData });
        }
        this.loaderclass.loader(false);
      });
    }

    global.isViewOpen = 1;
    BackAndroid.addEventListener("hardwareBackPress", this.onBackPress);
    this.loaderclass.loader(true);
  }
  tapExitUpdate() {
    this.setState({ tapExit: 0 });
    global.isViewOpen = 0;
  }
  onBackPress() {
    let route = this.props.navigation.state.routeName;
    if (route == "DashboardScreen") {
      if (global.isViewOpen == 0) {
        this.setState({ tapExit: 1 });
        //global.isViewOpen = 1;
      } else {
        BackHandler.exitApp();
      }
    }
  }
  sendNotifyMe() {
    if (this.state.textInput === "") {
      this.dropdown.alertWithType("warn", "Warning", "Enter Comments");
    } else {
      this.loaderclass.loader(true);
      let user_data = this.state.notifyFilterItems;
      user_data.user_id = global.userDetails.id;
      user_data.message = this.state.textInput;

      Agent.CarAPIs.notifyMeApi(user_data).then(response => {
        if (response.data.status) {
          this.dropdown.alertWithType(
            "success",
            "success",
            "Comments successfully add"
          );
          this.setState({
            isNotifyView: false
          });
        } else {
          this.dropdown.alertWithType("error", "Error", response.data.msg);
        }
        this.loaderclass.loader(false);
      });
    }
  }
  _onRefresh = () => {
    this.setState({ refreshing: true });
    Agent.CarAPIs.getCarList().then(response => {
      if (response.data.status) {
        this.setState({
          data: response.data.data,
          refreshing: false,
          page: initialPage
        });
      }
    });
  };
  onEndReached = () => {
    let page = this.state.page;
    if (!this.onEndReachedCalledDuringMomentum) {
      Agent.CarAPIs.getCarList({ page: page }).then(response => {
        if (response.data.status) {
          this.setState({
            data: [...this.state.data, ...response.data.data],
            page: page + 1
          });
          this.onEndReachedCalledDuringMomentum = true;
        }
      });
    }
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={[styles.wrapper, { flex: 1 }]}>
        <Menu
          visible={this.state.menuVisible}
          onVisible={isVisible => {
            this.state.menuVisible = isVisible;
          }}
          data={[
            {
              title: "Update Profile",
              onPress: () => {
                global.isViewOpen = 0;
                this.setState({ tapExit: 0 });
                this.props.navigation.navigate("UpdateProfile");
              }
            },
            {
              title: "Logout",
              onPress: () => {
                this.props.navigation.navigate("Logout");
              }
            }
          ]}
        />
        <View
          style={{
            position: "absolute",
            left: 0,
            flex: 1,
            height: 64,
            top: 0,
            right: 0,
            backgroundColor: kThemeColor
          }}
        >
          <Text
            style={{
              color: "white",
              fontSize: 18,
              fontFamily: "muli-Regular",
              textAlign: "center",
              paddingVertical: 30
            }}
          >
            Dashboard
          </Text>
          <TouchableOpacity
            style={{
              position: "absolute",
              right: 2,
              top: 20
            }}
            onPress={() => this.setState({ menuVisible: true })}
          >
            <Image
              source={require("../../assets/Images/dotsIcon.png")}
              style={{
                width: 25,
                height: 25,
                marginVertical: 10,
                marginHorizontal: 0.5
              }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              position: "absolute",
              right: 35,
              top: 20
            }}
            onPress={() => {
              this.setState({ tapExit: 0 });
              global.isViewOpen = 0;
              navigate("FilterScreen");
            }}
          >
            <Image
              source={require("../../assets/Images/filter.png")}
              style={{
                width: 20,
                height: 20,
                marginVertical: 12,
                marginHorizontal: 0.5
              }}
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              position: "absolute",
              right: 65,
              top: 20
            }}
            onPress={() => {
              this.setState({ tapExit: 0 });
              global.isViewOpen = 0;
              navigate("Notification");
            }}
          >
            <Image
              source={require("../../assets/Images/notification.png")}
              style={{
                width: 20,
                height: 20,
                marginVertical: 12,
                marginHorizontal: 0.5
              }}
            />
          </TouchableOpacity>
        </View>

        {this.state.isNotifyView ? (
          <KeyboardAwareScrollView
            style={{
              position: "absolute",
              left: 0,
              top: 20,
              right: 0,
              height: "100%",
              backgroundColor: "white"
            }}
            enableOnAndroid={false}
          >
            <Text
              style={{
                color: "black",
                fontSize: 16,
                fontFamily: "muli-Bold",
                textAlign: "center",
                marginTop: 20,
                paddingVertical: 30
              }}
            >
              Oops, No Result found with Searched Criteria
            </Text>
            <TouchableHighlight
              style={{
                position: "absolute",
                right: 10,
                top: 10,
                width: 30,
                height: 30,
                backgroundColor: "red",
                justifyContent: "center",
                alignItems: "center"
              }}
              onPress={() => {
                this.setState({ isNotifyView: false });
              }}
            >
              <Text
                style={
                  ([styles.footerButtonTextStyle],
                  { color: "white", fontSize: 20 })
                }
              >
                ✕
              </Text>
            </TouchableHighlight>

            {this.state.filterListData.map(b => (
              <View style={[CarDetailsStyle.VehicalDetailsStyle]}>
                <Text
                  style={[
                    CarDetailsStyle.VehicalDetailsInViewTextStyle,
                    { fontFamily: "Muli-Bold" }
                  ]}
                >
                  {b.keyName}
                </Text>
                <Text
                  style={[
                    CarDetailsStyle.VehicalDetailsInViewTextStyle,
                    { marginLeft: 1 }
                  ]}
                >
                  {b.value}
                </Text>
              </View>
            ))}
            <Text
              style={{
                color: "black",
                fontSize: 16,
                fontFamily: "muli-Bold",
                textAlign: "left",
                padding: 20,
                paddingBottom: 5
              }}
            >
              Comments
            </Text>

            <TextInput
              autoCapitalize={"none"}
              autoCorrect={false}
              multiline={true}
              underlineColorAndroid={"transparent"}
              placeholder="Enter Information"
              style={{
                marginHorizontal: 20,
                flex: 1,
                height: 90,
                borderColor: "gray",
                borderWidth: 1,
                marginBottom: 20,
                padding: 5
              }}
              onChangeText={text => this.setState({ textInput: text })}
              //value={this.state.textInput}
            />
            {/* Login button view  */}
            <View style={[CarDetailsStyle.footerButtonView]}>
              {/* login button */}
              <TouchableHighlight
                style={[styles.buttonStyle, { backgroundColor: kThemeColor }]}
                onPress={this.sendNotifyMe}
              >
                <Text
                  style={([styles.footerButtonTextStyle], { color: "white" })}
                >
                  Notify Me
                </Text>
              </TouchableHighlight>
            </View>
          </KeyboardAwareScrollView>
        ) : (
          <FlatList
            style={{ marginTop: 64 }}
            data={this.state.data}
            renderItem={({ item }) => (
              <CarList
                {...this.props}
                item={item}
                tapExitUpdate={this.tapExitUpdate}
              />
            )}
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
            onEndReached={this.onEndReached.bind(this)}
            onEndReachedThreshold={0.5}
            onMomentumScrollBegin={() => {
              this.onEndReachedCalledDuringMomentum = false;
            }}
          />
        )}
        <DropdownAlert
          ref={ref => (this.dropdown = ref)}
          closeInterval={1500}
        />
        <LoaderClass
          onRef={ref => {
            this.loaderclass = ref;
          }}
        />
        <PushNotification {...this.props} />
      </View>
    );
  }
}
