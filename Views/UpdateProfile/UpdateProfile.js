import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  TextInput,
  TouchableHighlight
} from "react-native";
import { styles, FormStyle } from "../../configs/Styles";
import Agent from "../../configs/Agent";
import DropdownAlert from "react-native-dropdownalert";
import Store from "react-native-simple-store";
import LoaderClass from "../../configs/LoaderClass";
export default class UpdateProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      userName: "",
      userEmail: "",
      userMobile: "",
      userOldPassword: "",
      userNewPassword: "",
      userConfPassword: ""
    };
    this.handleUpdateProfile = this.handleUpdateProfile.bind(this);
  }
  componentDidMount() {
    Store.get("auth").then(res => {
      global.userDetails = res;
      if (global.userDetails) {
        //alert(JSON.stringify(global.userDetails));
        this.setState({
          userId: global.userDetails.id,
          userName: global.userDetails.name,
          userEmail: global.userDetails.email,
          userMobile: global.userDetails.mobile
        });
      }
    });
  }

  handleUpdateProfile() {
    if (this.state.userName === "") {
      this.dropdown.alertWithType("error", "", "Please Enter Fullname");
      return false;
    } else if (
      this.state.userNewPassword !== "" ||
      this.state.userOldPassword !== ""
    ) {
      if (this.state.userOldPassword === "") {
        this.dropdown.alertWithType("error", "", "Please Enter Old Password");
        return false;
      } else if (this.state.userNewPassword === "") {
        this.dropdown.alertWithType("error", "", "Please Enter New Password");
        return false;
      } else if (this.state.userNewPassword !== this.state.userConfPassword) {
        this.dropdown.alertWithType(
          "error",
          "",
          "New & Confirm Password Does't Match"
        );
        return false;
      }
    }
    this.loaderclass.loader(true);
    let postData = {
      user_id: this.state.userId,
      name: this.state.userName,
      //email: this.state.userEmail,
      //mobile: this.state.userMobile,
      old_password: this.state.userOldPassword,
      new_password: this.state.userNewPassword
    };
    Agent.UserApi.updateProfile(postData).then(response => {
      global.userDetails = response.data.data;
      if (response.data.status) {
        Store.update("auth", global.userDetails);
        this.dropdown.alertWithType("success", "Success", response.data.msg);
      } else {
        this.dropdown.alertWithType("error", "Error", response.data.msg);
      }
      this.loaderclass.loader(false);
    });
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={[styles.wrapper, { flex: 1 }]}>
        <ScrollView>
          <View style={FormStyle.textInputsView}>
            <Text style={FormStyle.label}>Fullname</Text>
            <TextInput
              style={FormStyle.textinput}
              autoCorrect={false}
              autoCapitalize={"none"}
              placeholder="Enter Fullname"
              underlineColorAndroid={"transparent"}
              onChangeText={userName => {
                this.setState({ userName });
              }}
              value={this.state.userName}
            />
          </View>
          <View style={FormStyle.textInputsView}>
            <Text style={FormStyle.label}>E-mail Address</Text>
            <TextInput
              autoCapitalize={"none"}
              autoCorrect={false}
              keyboardType="email-address"
              style={FormStyle.textinput}
              placeholder="Enter E-mail Address"
              underlineColorAndroid={"transparent"}
              onChangeText={userEmail => {
                this.setState({ userEmail });
              }}
              value={this.state.userEmail}
              editable={false}
            />
          </View>
          <View style={FormStyle.textInputsView}>
            <Text style={FormStyle.label}>Mobile Number</Text>
            <TextInput
              autoCapitalize={"none"}
              autoCorrect={false}
              style={[FormStyle.textinput]}
              placeholder="Enter Mobile"
              underlineColorAndroid={"transparent"}
              onChangeText={userMobile => {
                this.setState({ userMobile });
              }}
              value={this.state.userMobile}
              editable={false}
            />
          </View>
          <View style={FormStyle.textInputsView}>
            <Text style={FormStyle.label}>Old Password</Text>
            <TextInput
              autoCapitalize={"none"}
              autoCorrect={false}
              style={[FormStyle.textinput]}
              placeholder="Enter Old Password"
              underlineColorAndroid={"transparent"}
              secureTextEntry={true}
              onChangeText={userOldPassword => {
                this.setState({ userOldPassword });
              }}
            />
          </View>
          <View style={FormStyle.textInputsView}>
            <Text style={FormStyle.label}>New Password</Text>
            <TextInput
              autoCapitalize={"none"}
              autoCorrect={false}
              style={[FormStyle.textinput]}
              placeholder="Enter New Password"
              underlineColorAndroid={"transparent"}
              secureTextEntry={true}
              onChangeText={userNewPassword => {
                this.setState({ userNewPassword });
              }}
            />
          </View>
          <View style={FormStyle.textInputsView}>
            <Text style={FormStyle.label}>Confirm Password</Text>
            <TextInput
              autoCapitalize={"none"}
              autoCorrect={false}
              style={[FormStyle.textinput]}
              placeholder="Enter Confirm Password"
              underlineColorAndroid={"transparent"}
              secureTextEntry={true}
              onChangeText={userConfPassword => {
                this.setState({ userConfPassword });
              }}
            />
          </View>
          <View style={FormStyle.btnView}>
            <TouchableHighlight
              style={FormStyle.btn}
              underlayColor="transparent"
              onPress={this.handleUpdateProfile}
            >
              <Text style={FormStyle.btnText}> Update </Text>
            </TouchableHighlight>
          </View>
        </ScrollView>
        <DropdownAlert
          ref={ref => (this.dropdown = ref)}
          closeInterval={1500}
        />
        <LoaderClass
          onRef={ref => {
            this.loaderclass = ref;
          }}
        />
      </View>
    );
  }
}
