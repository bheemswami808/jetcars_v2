import React, { Component } from "react";
import { View, BackAndroid } from "react-native";
import Agent from "../../configs/Agent";
import Store from "react-native-simple-store"; //local storage
//import RootLogin from "../../configs/router_login";
import LoaderClass from "../../configs/LoaderClass";
export default class LogoutScreen extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    this.loaderclass.loader(true);
    //BackAndroid.addEventListener("hardwareBackPress", this.handleBackButton);
    Agent.UserApi.logoutUser(global.userDetails.id).then(response => {
      if (response.data.status) {
        Store.update("auth", "");
        this.props.navigation.navigate("Login");
      } else {
        this.props.navigation.goBack();
      }
    });
  }
  componentWillUnmount() {
    //BackAndroid.removeEventListener("hardwareBackPress", this.handleBackButton);
  }
  handleBackButton() {
    BackHandler.exitApp();
    return true;
  }
  render() {
    return (
      <LoaderClass
        onRef={ref => {
          this.loaderclass = ref;
        }}
      />
    );
  }
}
